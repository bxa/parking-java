package com.services;

import com.dao.UserDao;
import com.models.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserService {
    private final UserDao userDao;

    @Autowired
    public UserService(UserDao userDao) {
        this.userDao = userDao;
    }

    public boolean add(User user) {
        return this.userDao.add(user);
    }

    public boolean update(User user, User obj) {
        return this.userDao.save(user, obj);
    }

    public int delete(int id, Boolean flag) {
        return this.userDao.delete(id);
    }

    public List<User> get(String queryString) {
        return this.userDao.get(queryString).list();
    }

    public User get(Integer id) {return this.userDao.get(id);}

    public List<User> getBy(String key, String value) {
        return this.userDao.getBy(key, value);
    }
    public boolean save(User newUser, User user) {
        return this.userDao.save(newUser, user);
    }
    public Boolean exists(String key, String value) {
        return this.userDao.exists(key, value);
    }

    public boolean save(User newUser) {
        return this.userDao.save(newUser);
    }

    public Boolean isMobileExist(String mobile) {
        return this.userDao.isMobileExist(mobile);
    }

    public User findOrCreate(String[] keyValue, UserDao userDao) {
        return this.userDao.findOrCreate(keyValue, userDao);
    }

    public User findBy(String column, String value) {
        return this.userDao.findBy(column, value);
    }

    public User getByMobile(String mobile) {
        return this.userDao.getBy("mobile", mobile).get(0);
    }
}
