package com.services;

import com.dao.OrderDao;
import com.models.Order;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class OrderService {
    @Autowired
    private final OrderDao orderDao;


    @Autowired
    public OrderService(OrderDao orderDao) {
        this.orderDao = orderDao;
    }

    @SuppressWarnings("unchecked")
    public Query get(String queryString) { return orderDao.get(queryString); }


    @SuppressWarnings("unchecked")
    public Query get(String queryString, String page) { return orderDao.get(queryString, page); }


    public Order get(int id) { return (Order) orderDao.get(id); }

    public int delete(int id) { return orderDao.delete(id); }


    public Integer counts(String queryString) { return orderDao.counts(queryString); }


    public Boolean exists(String key, String value) {return orderDao.exists(key,value);}

}
