package com.dao;

import com.models.Transaction;
import org.hibernate.query.Query;

public interface TransactionDao {
    int delete(int id);                         // delete var / change status to 0
    Transaction get(int id);                                // fetch one var
    Query get(String queryString);              // fetch list of vars
    Query get(String queryString, String offset);              // fetch list of vars
    Integer counts(String queryString);
    Query countsQuery(String queryString);
    Boolean exists(String key, String value);
    Boolean exists(String[] keys, String[] values);
    Boolean existsExept(String plateNumber, Transaction var);

    Boolean add(Transaction var);                           // insert into database
    Boolean save(Transaction var, Transaction obj);                  // save with having Id
    Boolean save(Transaction var);

    javax.persistence.Query getQuery(String queryString, String page);
}
