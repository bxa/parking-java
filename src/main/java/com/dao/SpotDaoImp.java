package com.dao;

import com.controllers.api.SpotController;
import com.models.Spot;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;


@Repository("spotDao")
@Transactional
public class SpotDaoImp implements SpotDao {

    @Value("${itemsPerPage}")
    private int itemsPerPage;
    private  static final Logger logger = LoggerFactory.getLogger(SpotController.class);
    private final SessionFactory sessionFactory;
    private ModelDao modelDao;

    @Autowired
    public SpotDaoImp(SessionFactory sessionFactory, ModelDao modelDao) {
        this.sessionFactory = sessionFactory;
        this.modelDao = modelDao;
    }


    @Override
    @SuppressWarnings("unchecked")
    public Query get(String queryString) { return modelDao.get(queryString, "spots"); }


    @Override
    @SuppressWarnings("unchecked")
    public Query get(String queryString, String page) { return modelDao.get(queryString, page, "spots");}

    @Override
    @SuppressWarnings("unchecked")
    public javax.persistence.Query getQuery(String queryString, String page) { return modelDao.getQuery(Spot.class, queryString, page);}


    @Override
    public Spot get(int id) { return (Spot) modelDao.find(Spot.class, id); }

    @Override
    public int delete(int id) { return modelDao.delete(Spot.class, id); }


    @Override
    public Integer counts(String queryString) { return modelDao.counts(queryString, "spots"); }

    @Override
    public Query countsQuery(String queryString) { return modelDao.countsQuery(queryString, "spots"); }

    @Override
    public Boolean exists(String key, String value) {return modelDao.exists("spots",key,value);}

    @Override
    public Boolean exists(String[] keys, String[] values) {return modelDao.exists("spots",keys,values);}

    @Override
    public Boolean existsExept(String plateNumber, Spot entity) { return modelDao.existsExept("spots", "plate_number", plateNumber, entity.getId()); }


    /**
     * change entity database
     */
    @Override
    public Boolean save(Spot entity, Spot obj) {
        Session session = this.sessionFactory.getCurrentSession();
        try {
            if(entity.getOrders() != null) obj.setOrders(entity.getOrders());
            if(entity.getLoraSerial() != null) obj.setLoraSerial(entity.getLoraSerial());
            if(entity.getLot() != null) obj.setLot(entity.getLot());
            if(entity.getStatus() != null) obj.setStatus(entity.getStatus());
            if(entity.getType() != null) obj.setType(entity.getType());

            session.update(obj);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }
    /**
     * change entity database
     */
    @Override
    public Boolean save(Spot entity, int id) {
        Session session = this.sessionFactory.getCurrentSession();
        try {
            Spot obj = session.get(Spot.class, id);
            System.out.println("this is objjjjjjjjjjjjjjjj: ");
            System.out.println(obj);
            if(entity.getOrders() != null) obj.setOrders(entity.getOrders());
            if(entity.getLoraSerial() != null) obj.setLoraSerial(entity.getLoraSerial());
            if(entity.getLot() != null) obj.setLot(entity.getLot());
            if(entity.getStatus() != null) obj.setStatus(entity.getStatus());
            if(entity.getType() != null) obj.setType(entity.getType());

            System.out.println("this is obj: ");
            System.out.println(obj);
            session.update(obj);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * insert entity database
     */
    @Override
    @SuppressWarnings("unchecked")
    public Boolean save(Spot entity) {
        Session session = this.sessionFactory.getCurrentSession();
        try {
            session.save(entity);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * add entity to database
     */
    @Override
    public Boolean add(Spot entity) {
        try {
            Session session = this.sessionFactory.getCurrentSession();
            session.persist(entity);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }
}
