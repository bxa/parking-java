package com.dao;

import com.controllers.api.UserController;
import com.models.User;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository("userDao")
@Transactional
public class UserDaoImp implements UserDao {

    private  static final Logger logger = LoggerFactory.getLogger(UserController.class);
	private final SessionFactory sessionFactory;
    private ModelDao modelDao;
    private static final String table = "users";

    @Autowired
    public UserDaoImp(SessionFactory sessionFactory, ModelDao modelDao) {
        this.sessionFactory = sessionFactory;
        this.modelDao = modelDao;
	}

    @Override
    @SuppressWarnings("unchecked")
    public Query get(String queryString) {
        return modelDao.get(queryString, table);
    }


    @Override
    @SuppressWarnings("unchecked")
    public Query get(String queryString, String page) {
        return modelDao.get(queryString, page, table);
    }

    @Override
    @SuppressWarnings("unchecked")
    public javax.persistence.Query getQuery(String queryString, String page) {
        return modelDao.getQuery(User.class, queryString, page);
    }

    @Override
    public User get(int id) {
        return (User) modelDao.find(User.class, id);
    }

    @Override
    public int delete(int id) {
        return modelDao.delete(User.class, id);
    }

    @Override
    public Integer counts(String queryString) {
        return modelDao.counts(queryString, table);
    }

    @Override
    public Query countsQuery(String queryString) {
        return modelDao.countsQuery(queryString, table);
    }

    @Override
    public Boolean exists(String key, String value) {
        return modelDao.exists(table, key, value);
    }

    @Override
    public Boolean exists(String[] keys, String[] values) {
        return modelDao.exists(table, keys, values);
    }



	public List<User> getBy(String column, String value) {
		Session session = this.sessionFactory.getCurrentSession();
		try {
			return session.createQuery("from users where "+column+" = :col").setParameter("col", value).list();
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	/**
	* add user to database
	 */
	@Override
	public Boolean add(User user) {
		try {
			Session session = this.sessionFactory.getCurrentSession();
			session.persist(user);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	/**
	 * change user database
	 */
	@Override
	public Boolean save(User user, User obj) {
		Session session = this.sessionFactory.getCurrentSession();
		try {
//			User obj = session.get(User.class, id);
//			if(user.getToken() != null) obj.setToken(user.getToken());
//			if(user.getStatus() != true || user.getStatus() != false ) obj.setStatus(user.getStatus());
			if(user.getMobile() != null) obj.setMobile(user.getMobile());
			if(user.getConfirmMobile() != null) obj.setConfirmMobile(user.getConfirmMobile());
			if(user.getName() != null) obj.setName(user.getName());
			if(user.getStatus() != null) obj.setStatus(user.getStatus());
			if(user.getType() != null) obj.setType(user.getType());

//			System.out.println("update users set mobile=" + obj.getMobile() + ", token=" + obj.getToken() + ", confirmmobile=" + obj.getConfirmMobile() + ", name=" + obj.getName() + ", status=" + obj.getStatus() + ", status=" + obj.getStatus() + ", type=" + obj.getType() + ", email=" + obj.getEmail() + ", password=" + obj.getPassword() + ", plate_number=" + obj.getPlateNumber() + ", last_factor=" + obj.getLastFactor() + ", paid_status=" + obj.getPaidStatus() + ", order_count=" + obj.getOrderCount() + ", avatar_link=" + obj.getAvatarLink() + ", remember_token=" + obj.getRememberToken());
			try {
				session.saveOrUpdate(obj);
//				session.flush();
				return true;
			} catch (Exception e) {
				System.out.println(e.getMessage());
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	/**
	 * insert user database
	 */
	@Override
	public Boolean save(User user) {
		Session session = this.sessionFactory.getCurrentSession();
		try {
			session.save(user);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}


	@Override
	public Boolean isMobileExist(String mobile){
		Session session = this.sessionFactory.getCurrentSession();
		if (session.createQuery("from users WHERE mobile = " + mobile).list().isEmpty())
		    return false;
		else
		    return true;
	}

	@Override
	public User findOrCreate(String[] keyValue, UserDao userDao)
	{
		Session session = this.sessionFactory.getCurrentSession();
		User user = new User();
		if (session.createQuery("from users WHERE " + keyValue[0] + " = " + keyValue[1]).list().isEmpty()) {
			user.setMobile(keyValue[1]);
			userDao.add(user);
			userDao.save(user, user);
		} else {
			user =(User) userDao.get(" WHERE " + keyValue[0] + " = " + keyValue[1]).list().get(0);
		}
		return user;
	}

	@Override
	public User findBy(String column, String value)
	{
		Session session = this.sessionFactory.getCurrentSession();
		User user = new User();

		user = (User) this.get(" WHERE " + column + " = " + value).list().get(0);
//		user = session.createQuery("from users WHERE " + column + " = :" + column)
//				.setParameter(":"+column, value).get(0);
//		q.(":"+column, value);
		return user;
	}
}
