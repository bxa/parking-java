package com.dao;

import com.controllers.api.ReportController;
import com.models.Report;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;


@Repository("reportDao")
@Transactional
public class ReportDaoImp implements ReportDao {

    @Value("${itemsPerPage}")
    private int itemsPerPage;
    private  static final Logger logger = LoggerFactory.getLogger(ReportController.class);
    private final SessionFactory sessionFactory;
    private ModelDao modelDao;

    @Autowired
    public ReportDaoImp(SessionFactory sessionFactory, ModelDao modelDao) {
        this.sessionFactory = sessionFactory;
        this.modelDao = modelDao;
    }


    @Override
    @SuppressWarnings("unchecked")
    public Query get(String queryString) { return modelDao.get(queryString, "reports"); }


    @Override
    @SuppressWarnings("unchecked")
    public Query get(String queryString, String page) { return modelDao.get(queryString, page, "reports");}

    @Override
    @SuppressWarnings("unchecked")
    public javax.persistence.Query getQuery(String queryString, String page) { return modelDao.getQuery(Report.class, queryString, page);}


    @Override
    public Report get(int id) { return (Report) modelDao.find(Report.class, id); }

    @Override
    public int delete(int id) { return modelDao.delete(Report.class, id); }


    @Override
    public Integer counts(String queryString) { return modelDao.counts(queryString, "reports"); }

    @Override
    public Query countsQuery(String queryString) { return modelDao.countsQuery(queryString, "reports"); }


    @Override
    public Boolean exists(String plateNumber) {return modelDao.exists("reports","plate_number",plateNumber);}

    @Override
    public Boolean exists(String key, String value) {return modelDao.exists("reports",key,value);}

    @Override
    public Boolean exists(String[] keys, String[] values) {return modelDao.exists("reports",keys,values);}

    @Override
    public Boolean existsExept(String plateNumber, Report report) { return modelDao.existsExept("reports", "plate_number", plateNumber, report.getId()); }


    /**
     * change report database
     */
    @Override
    public Boolean save(Report report, Report obj) {
        Session session = this.sessionFactory.getCurrentSession();
        try {
//            if(report.getItem_id() != null) obj.setItem_id(report.getItem_id());
            if(report.getType() != null) obj.setType(report.getType());
            if(report.getStatus() != null) obj.setStatus(report.getStatus());

            session.update(obj);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * insert report database
     */
    @Override
    @SuppressWarnings("unchecked")
    public Boolean save(Object report) {
        Session session = this.sessionFactory.getCurrentSession();
        try {
            session.save(report);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * add report to database
     */
    @Override
    public Boolean add(Report report) {
        try {
            Session session = this.sessionFactory.getCurrentSession();
            session.persist(report);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }
}
