package com.dao;

import com.models.Spot;
import org.hibernate.query.Query;

import java.util.List;

public interface SpotDao {
    int delete(int id);                         // delete var / change status to 0
    Spot get(int id);                                // fetch one var
    Query get(String queryString);              // fetch list of vars
    Query get(String queryString, String offset);              // fetch list of vars
    Integer counts(String queryString);
    Query countsQuery(String queryString);
    Boolean exists(String key, String value);
    Boolean exists(String[] keys, String[] values);
    Boolean existsExept(String plateNumber, Spot var);

    Boolean add(Spot var);                           // insert into database
    Boolean save(Spot var, Spot obj);                  // save with having Id
    Boolean save(Spot var, int obj);                  // save with having Id
    Boolean save(Spot var);

    javax.persistence.Query getQuery(String queryString, String page);
}
