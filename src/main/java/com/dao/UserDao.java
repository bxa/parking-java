package com.dao;

import com.models.User;
import org.hibernate.query.Query;

import java.util.List;

public interface UserDao {
	int delete(int id);                         // delete var / change status to 0
    User get(int id);                                // fetch one var
    Query get(String queryString);              // fetch list of vars
    Query get(String queryString, String offset);              // fetch list of vars
    Integer counts(String queryString);
    Query countsQuery(String queryString);
    Boolean exists(String key, String value);
    Boolean exists(String[] keys, String[] values);

    javax.persistence.Query getQuery(String queryString, String page);

	Boolean add(User user);
	Boolean save(User user, User obj);
	Boolean save(User user);
	Boolean isMobileExist(String mobile);
	User findOrCreate(String[] keyValue, UserDao userDao);
	User findBy(String column, String value);
	List<User> getBy(String column, String value);
}
