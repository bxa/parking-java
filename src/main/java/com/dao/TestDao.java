package com.dao;

import org.hibernate.query.Query;

public interface TestDao {
    Query get(String query);
}
