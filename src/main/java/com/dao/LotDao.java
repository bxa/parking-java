package com.dao;

import com.models.Lot;
import org.hibernate.query.Query;

public interface LotDao {
    int delete(int id);                         // delete lot / change status to 0
    Lot get(int id);                                // fetch one lot
    Query get(String queryString);              // fetch list of lots
    Query get(String queryString, String offset);              // fetch list of lots
    Integer counts(String queryString);
    Query countsQuery(String queryString);
    Boolean exists(String plateNumber);
    Boolean exists(String key, String value);
    Boolean existsExept(String plateNumber, Lot lot);

    Boolean add(Lot lot);                           // insert into database
    Boolean save(Lot lot, Lot obj);                  // save with having Id
    Boolean save(Lot lot);

    javax.persistence.Query getQuery(String where, String page);
}
