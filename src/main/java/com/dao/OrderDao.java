package com.dao;

import com.models.Order;
import org.hibernate.query.Query;


public interface OrderDao {
    int delete(int id);                         // delete var / change status to 0
    Order get(int id);                                // fetch one var
    Query get(String queryString);              // fetch list of vars
    Query get(String queryString, String offset);              // fetch list of vars
    Integer counts(String queryString);
    Query countsQuery(String queryString);
    Boolean exists(String key, String value);
    Boolean exists(String[] keys, String[] values);

    Boolean add(Order var);                           // insert into database
    Boolean save(Order var, Order obj);                  // save with having Id
    Boolean save(Order var);

    javax.persistence.Query getQuery(String queryString, String page);

}
