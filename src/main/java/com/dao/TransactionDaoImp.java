package com.dao;

import com.controllers.api.TransactionController;
import com.models.Transaction;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;


@Repository("entityDao")
@Transactional
public class TransactionDaoImp implements TransactionDao {

    @Value("${itemsPerPage}")
    private int itemsPerPage;
    private  static final Logger logger = LoggerFactory.getLogger(TransactionController.class);
    private final SessionFactory sessionFactory;
    private ModelDao modelDao;

    @Autowired
    public TransactionDaoImp(SessionFactory sessionFactory, ModelDao modelDao) {
        this.sessionFactory = sessionFactory;
        this.modelDao = modelDao;
    }


    @Override
    @SuppressWarnings("unchecked")
    public Query get(String queryString) { return modelDao.get(queryString, "transactions"); }


    @Override
    @SuppressWarnings("unchecked")
    public Query get(String queryString, String page) { return modelDao.get(queryString, page, "transactions");}

    @Override
    @SuppressWarnings("unchecked")
    public javax.persistence.Query getQuery(String queryString, String page) { return modelDao.getQuery(Transaction.class, queryString, page);}


    @Override
    public Transaction get(int id) { return (Transaction) modelDao.find(Transaction.class, id); }

    @Override
    public int delete(int id) { return modelDao.delete(Transaction.class, id); }


    @Override
    public Integer counts(String queryString) { return modelDao.counts(queryString, "transactions"); }

    @Override
    public Query countsQuery(String queryString) { return modelDao.countsQuery(queryString, "transactions"); }

    @Override
    public Boolean exists(String key, String value) {return modelDao.exists("transactions",key,value);}

    @Override
    public Boolean exists(String[] keys, String[] values) {return modelDao.exists("transactions",keys,values);}

    @Override
    public Boolean existsExept(String plateNumber, Transaction entity) { return modelDao.existsExept("transactions", "plate_number", plateNumber, entity.getId()); }


    /**
     * change entity database
     */
    @Override
    public Boolean save(Transaction entity, Transaction obj) {
        Session session = this.sessionFactory.getCurrentSession();
        try {
            if(entity.getType() != null) obj.setType(entity.getType());
            if(entity.getStatus() != null) obj.setStatus(entity.getStatus());
            if(entity.getUser() != null) obj.setUser(entity.getUser());
            if(entity.getAmount() != null) obj.setAmount(entity.getAmount());

            session.update(obj);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * insert entity database
     */
    @Override
    @SuppressWarnings("unchecked")
    public Boolean save(Transaction entity) {
        Session session = this.sessionFactory.getCurrentSession();
        try {
            session.save(entity);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * add entity to database
     */
    @Override
    public Boolean add(Transaction entity) {
        try {
            Session session = this.sessionFactory.getCurrentSession();
            session.persist(entity);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }
}
