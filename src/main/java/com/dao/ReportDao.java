package com.dao;

import com.models.Report;
import org.hibernate.query.Query;

import java.util.HashMap;

public interface ReportDao {
    int delete(int id);                         // delete report / change status to 0
    Report get(int id);                                // fetch one report
    Query get(String queryString);              // fetch list of reports
    Query get(String queryString, String offset);              // fetch list of reports
    Integer counts(String queryString);
    Query countsQuery(String queryString);
    Boolean exists(String plateNumber);
    Boolean exists(String key, String value);
    Boolean exists(String[] keys, String[] values);
    Boolean existsExept(String plateNumber, Report report);

    Boolean add(Report report);                           // insert into database
    Boolean save(Report report, Report obj);                  // save with having Id
    Boolean save(Object report);

    javax.persistence.Query getQuery(String where, String page);
}
