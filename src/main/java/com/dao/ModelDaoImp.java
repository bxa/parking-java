package com.dao;

import com.controllers.api.config.JPAUtil;
import com.controllers.web.HelperController;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.*;

@Repository("modelDao")
@Transactional
public class ModelDaoImp implements ModelDao {

    @Value("${itemsPerPage}")
    private int itemsPerPage;
    private final SessionFactory sessionFactory;
//    private ModelDao modelDao;
//    private EntityManagerFactory emf = Persistence.createEntityManagerFactory("PERSISTENCE");
//    @PersistenceContext
//    private EntityManager em = JPAUtil.get();


    @Autowired
    public ModelDaoImp(SessionFactory sessionFactory)
    {
        this.sessionFactory = sessionFactory;
//        EntityManager em = JPAUtil.get();
//        this.emf =  Persistence.createEntityManagerFactory("MyTest");
//        this.modelDao = modelDao;
    }
//
//    public EntityManager getEntityManager() {
//        return emf.createEntityManager();
//    }

    public Session getNotDeletedRowsSession()
    {
        Session session = this.sessionFactory.getCurrentSession();
        session.enableFilter("deleted").setParameter("deleted", true);
        return session;
    }

    @Override
    @SuppressWarnings("unchecked")
    public Query get(String queryString, String table)
    {
        Session session = this.getNotDeletedRowsSession();
        try {
            return session.createQuery("from " + table + " " + queryString);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    @SuppressWarnings("unchecked")
    public Query get(String queryString, String offset, String table)
    {
        HelperController hc = new HelperController();
        int page = hc.getPage(offset);

        Session session = this.getNotDeletedRowsSession();
        try {
            return session.createQuery("from " + table + " " + queryString)
                    .setFirstResult(page*this.itemsPerPage).setMaxResults(this.itemsPerPage)
                    ;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    @SuppressWarnings("unchecked")
    public javax.persistence.Query getQuery(Class cls, String queryString, String offset)
    {
        HelperController hc = new HelperController();
        int page = hc.getPage(offset);

        Session session = this.getNotDeletedRowsSession();
//        emf = Persistence.createEntityManagerFactory("MyTest");

        try {
            System.out.println("this is the query string : "+ queryString);
            return session.createNativeQuery(queryString, cls).
            setFirstResult(page*this.itemsPerPage).setMaxResults(this.itemsPerPage);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public Integer counts(String query, String table)
    {
        Session session = this.getNotDeletedRowsSession();
        Long counts = (Long) session.createQuery("SELECT count(*) from  " + table + " " + query).uniqueResult();
        return counts.intValue();
    }

    @Override
    public Query countsQuery(String query, String table)
    {
        Session session = this.getNotDeletedRowsSession();
        Query counts = session.createNativeQuery(query);
        return counts;
    }


    @Override
    public Boolean exists(String table, String key, String value)
    {
        Session session = this.getNotDeletedRowsSession();
        Long counts = (Long) session.createQuery("SELECT count(*) from  " + table + " where "+ key + " = :value").setParameter("value", value).uniqueResult();
        Integer counts1 = counts.intValue();
        if (counts1 > 0)
            return true;
        else
            return false;
    }

    public Query bindParametersList(Query q, String[] values)
    {
        int i = 0;
        for(String value : values) {
            q.setParameter(i, value);
            i++;
        }
        return q;
    }

    @Override
    public Boolean exists(String table, String[] keys, String[] values)
    {
        String where = "";
        int i = 0;
        for(String key : keys) {
            where += key + "=?"+i+" and ";
            i++;
        }
        where = where.substring(0, where.length() - 5);

        Session session = this.getNotDeletedRowsSession();
        Query q =  session.createQuery("SELECT count(*) from  " + table + " where "+ where);
        q = this.bindParametersList(q, values);
        Long counts = (Long) q.uniqueResult();
        Integer counts1 = counts.intValue();
        if (counts1 > 0)
            return true;
        else
            return false;
    }

    @Override
    public Boolean existsExept(String table, String key, String value, Integer id)
    {
        Session session = this.getNotDeletedRowsSession();
//        Boolean carNotExists = !this.get(" where id != " + id + " and plate_number = :plate").setParameter("plate", plateNumber).list().isEmpty();
        Long counts = (Long) session.createQuery("SELECT count(*) from  " + table + " where id != " + id + " and "+ key + " = :value").setParameter("value", value).uniqueResult();
        Integer counts1 = counts.intValue();
        if (counts1 > 0)
            return true;
        else
            return false;
    }

    public Session getSession()
    {
        Session session = this.sessionFactory.getCurrentSession();
        return session;
    }

    public Object find(Class cls, int  id)
    {
        Session session = this.getNotDeletedRowsSession();
        return session.get(cls, id);
    }

    public int delete(Class cls, int id)
    {
        try {
            System.out.println("try to delete" + id);
            Session session = this.sessionFactory.getCurrentSession();
            return session.createNativeQuery("update cars set status = 0 where id = " + id).executeUpdate();
//            session.update(obj);
        } catch (Exception e) {
            System.out.println("try to delete error" + e.getMessage());
            return 0;
        }
    }
}
