package com.dao;

import com.models.Lot;
import com.controllers.api.LotController;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository("lotDao")
@Transactional
public class LotDaoImp implements LotDao {

    @Value("${itemsPerPage}")
    private int itemsPerPage;
    private  static final Logger logger = LoggerFactory.getLogger(LotController.class);
    private final SessionFactory sessionFactory;
    private ModelDao modelDao;

    @Autowired
    public LotDaoImp(SessionFactory sessionFactory, ModelDao modelDao) {
        this.sessionFactory = sessionFactory;
        this.modelDao = modelDao;
//        this.offset = @Value("${api.offset}");
    }


    @Override
    @SuppressWarnings("unchecked")
    public Query get(String queryString) { return modelDao.get(queryString, "lots"); }


    @Override
    @SuppressWarnings("unchecked")
    public Query get(String queryString, String page) { return modelDao.get(queryString, page, "lots");}

    @Override
    @SuppressWarnings("unchecked")
    public javax.persistence.Query getQuery(String queryString, String page) { return modelDao.getQuery(Lot.class, queryString, page);}


    @Override
    public Lot get(int id) { return (Lot) modelDao.find(Lot.class, id); }

    @Override
    public int delete(int id) { return modelDao.delete(Lot.class, id); }


    @Override
    public Integer counts(String queryString) { return modelDao.counts(queryString, "lots"); }

    @Override
    public Query countsQuery(String queryString) { return modelDao.countsQuery(queryString, "lots"); }


    @Override
    public Boolean exists(String plateNumber) {return modelDao.exists("lots","plate_number",plateNumber);}

    @Override
    public Boolean exists(String key, String value) {return modelDao.exists("lots",key,value);}


    @Override
    public Boolean existsExept(String plateNumber, Lot lot) { return modelDao.existsExept("lots", "plate_number", plateNumber, lot.getId()); }


    /**
     * change lot database
     */
    @Override
    public Boolean save(Lot lot, Lot obj) {
        Session session = this.sessionFactory.getCurrentSession();
        try {

            if(lot.getLotSpots() != null) obj.setLotSpots(lot.getLotSpots());
            if(lot.getAddressLot() != null) obj.setAddressLot(lot.getAddressLot());
            if(lot.getStatus() > 0) obj.setStatus(lot.getStatus());
            if(lot.getType() > 0) obj.setType(lot.getType());
            if(lot.getCapacity() > 0) obj.setCapacity(lot.getCapacity());
            if(lot.getName() != null) obj.setName(lot.getName());
            if(lot.getPhone() != null) obj.setPhone(lot.getPhone());
            if(lot.getId() != null) obj.setId(lot.getId());

            session.update(obj);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * insert lot database
     */
    @Override
    @SuppressWarnings("unchecked")
    public Boolean save(Lot lot) {
        Session session = this.sessionFactory.getCurrentSession();
        try {
            session.save(lot);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * add lot to database
     */
    @Override
    public Boolean add(Lot lot) {
        try {
            Session session = this.sessionFactory.getCurrentSession();
            session.persist(lot);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }
}
