package com.dao;

import com.models.Car;
import org.hibernate.query.Query;

import java.util.List;

public interface CarDao {
    int delete(int id);                         // delete car / change status to 0
    Car get(int id);                                // fetch one car
    Query get(String queryString);              // fetch list of cars
    Query get(String queryString, String offset);              // fetch list of cars
    Integer counts(String queryString);
    Boolean exists(String plateNumber);
    Boolean exists(String key, String value);
    Boolean existsExept(String plateNumber, Car car);

    Boolean add(Car car);                           // insert into database
    Boolean save(Car car, int id);                  // save with having Id
    Boolean save(Car car);                          // saving without having Id
}
