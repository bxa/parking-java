package com.dao;

import com.controllers.api.OrderController;
import com.models.Order;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;


@Repository("orderDao")
@Transactional
public class OrderDaoImp implements OrderDao {

    @Value("${itemsPerPage}")
    private int itemsPerPage;
    private static final Logger logger = LoggerFactory.getLogger(OrderController.class);
    private final SessionFactory sessionFactory;
    private ModelDao modelDao;
    private static final String table = "orders";

    @Autowired
    public OrderDaoImp(SessionFactory sessionFactory, ModelDao modelDao) {
        this.sessionFactory = sessionFactory;
        this.modelDao = modelDao;
//        this.offset = @Value("${api.offset}");
    }


    @Override
    @SuppressWarnings("unchecked")
    public Query get(String queryString) {
        return modelDao.get(queryString, table);
    }


    @Override
    @SuppressWarnings("unchecked")
    public Query get(String queryString, String page) {
        return modelDao.get(queryString, page, table);
    }

    @Override
    @SuppressWarnings("unchecked")
    public javax.persistence.Query getQuery(String queryString, String page) {
        return modelDao.getQuery(Order.class, queryString, page);
    }

    @Override
    public Order get(int id) {
        return (Order) modelDao.find(Order.class, id);
    }

    @Override
    public int delete(int id) {
        return modelDao.delete(Order.class, id);
    }


    @Override
    public Integer counts(String queryString) {
        return modelDao.counts(queryString, table);
    }

    @Override
    public Query countsQuery(String queryString) {
        return modelDao.countsQuery(queryString, table);
    }

    @Override
    public Boolean exists(String key, String value) {
        return modelDao.exists(table, key, value);
    }

    @Override
    public Boolean exists(String[] keys, String[] values) {
        return modelDao.exists(table, keys, values);
    }

    /**
     * add order to database
     */
    @Override
    public Boolean add(Order order) {
        try {
            Session session = this.sessionFactory.getCurrentSession();
            session.persist(order);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * change order database
     */
    @Override
    public Boolean save(Order order, Order obj) {
        Session session = this.sessionFactory.getCurrentSession();
        try {
            if (order.getType() != null) obj.setType(order.getType());
            if (order.getPrice() != null) obj.setPrice(order.getPrice());
            if (order.getStartTime() != null) obj.setStartTime(order.getStartTime());
            if (order.getEndTime() != null) obj.setEndTime(order.getEndTime());

            session.update(obj);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * insert order database
     */
    @Override
    @SuppressWarnings("unchecked")
    public Boolean save(Order order) {
        Session session = this.sessionFactory.getCurrentSession();
        try {
            session.save(order);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }
}
