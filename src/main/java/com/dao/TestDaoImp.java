package com.dao;


import com.controllers.api.OrderController;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.logging.Logger;


@Repository("testDao")
@Transactional
public class TestDaoImp implements TestDao {
    @Value("${itemsPerPage}")
    private int itemsPerPage;
//    private  static final Logger logger = LoggerFactory.getLogger(OrderController.class);
    private final SessionFactory sessionFactory;


    @Autowired
    public TestDaoImp(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public Query get(String query)
    {
        Session session = this.sessionFactory.getCurrentSession();
        try {
            return session.createQuery("from orders " + query);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

}
