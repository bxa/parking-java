package com.dao;

import org.hibernate.Session;
import org.hibernate.query.Query;

import java.util.HashMap;

public interface ModelDao {
    Object find(Class cls, int id);
    int delete(Class cls, int id);
    Session getNotDeletedRowsSession();
    Session getSession();
    Query get(String table, String query);
    Query get(String query, String page, String table);
    Integer counts(String query, String table);
    Query countsQuery(String query, String table);
    Boolean exists(String table, String key, String value);
    Boolean exists(String table, String[] keys, String[] values);
    Boolean existsExept(String table, String key, String value, Integer id);

    javax.persistence.Query getQuery(Class cls, String queryString, String page);
}
