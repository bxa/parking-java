package com.dao;

import com.controllers.api.CarController;
import com.models.Car;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository("carDao")
@Transactional

public class CarDaoImp implements CarDao {
    @Value("${itemsPerPage}")
    private int itemsPerPage;
    private  static final Logger logger = LoggerFactory.getLogger(CarController.class);
    private final SessionFactory sessionFactory;
    private ModelDao modelDao;

    @Autowired
    public CarDaoImp(SessionFactory sessionFactory, ModelDao modelDao) {
        this.sessionFactory = sessionFactory;
        this.modelDao = modelDao;
//        this.offset = @Value("${api.offset}");
    }


    @Override
    @SuppressWarnings("unchecked")
    public Query get(String queryString) { return modelDao.get(queryString, "cars"); }


    @Override
    @SuppressWarnings("unchecked")
    public Query get(String queryString, String page) { return modelDao.get(queryString, page, "cars");}


    @Override
    public Car get(int id) { return (Car) modelDao.find(Car.class, id); }

    @Override
    public int delete(int id) { return modelDao.delete(Car.class, id); }


    @Override
    public Integer counts(String queryString) { return modelDao.counts(queryString, "cars"); }


    @Override
    public Boolean exists(String plateNumber) {return modelDao.exists("cars","plate_number",plateNumber);}

    @Override
    public Boolean exists(String key, String value) {return modelDao.exists("cars",key,value);}


    @Override
    public Boolean existsExept(String plateNumber, Car car) { return modelDao.existsExept("cars", "plate_number", plateNumber, car.getId()); }


    /**
     * change car database
     */
    @Override
    public Boolean save(Car car, int id) {
        Session session = this.sessionFactory.getCurrentSession();
        try {
            Car obj = session.get(Car.class, id);
            if(car.getColor() != null) obj.setColor(car.getColor());
            if(car.getModelYear() != null) obj.setModelYear(car.getModelYear());
            if(car.getBrand() != null) obj.setBrand(car.getBrand());

            session.update(obj);
            return true;
        } catch (Exception e) {
            session.clear();
            e.printStackTrace();
            return false;
        }
    }

    /**
     * insert car database
     */
    @Override
    @SuppressWarnings("unchecked")
    public Boolean save(Car car) {
        Session session = this.sessionFactory.getCurrentSession();
        try {
            session.save(car);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * add car to database
     */
    @Override
    public Boolean add(Car car) {
        try {
            Session session = this.sessionFactory.getCurrentSession();
            session.persist(car);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

}
