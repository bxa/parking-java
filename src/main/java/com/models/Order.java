package com.models;

import com.fasterxml.jackson.annotation.JsonFilter;
import com.fasterxml.jackson.annotation.JsonIgnore;
//import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.ParamDef;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity(name = "orders")
@DynamicUpdate
@FilterDef(name="deleted", parameters=@ParamDef( name="deleted", type="boolean"))
@Filter(name = "deleted", condition = "deleted <> :deleted")
public class Order implements Serializable {
//	private static final long serialVersionUID = 1L;
	public Order() {}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonProperty
	private Integer id;

	private Long price;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "user_id", nullable = false)
	private User user;

//	@OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
//	@JoinColumn(name = "transaction_id", nullable = false)
//	@JsonIgnore
//	private Transaction transaction;

//	@OneToOne(mappedBy = "order", cascade = CascadeType.ALL)
//    private Transaction transaction;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name="spot_id")
	private Spot spot;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name="lot_id")
	private Lot lot;


	@Column(name="lot_id", updatable = false, insertable = false)
	private Integer lotId;

	@Column(name = "start_time")
	private Date startTime;

	@Column(name = "end_time", insertable=false)
	private Date endTime;

	@Column(insertable=false)
	private Integer status;

	@Column(insertable=false)
	private Integer type;

	@Column(name = "created_at", updatable = false, insertable=false)
	private java.util.Date createdAt;

	@Column(name = "updated_at", updatable = false, insertable = false)
	private java.util.Date updatedAt;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Long getPrice() {
		return price;
	}

	public void setPrice(Long price) {
		this.price = price;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Spot getSpot() {
		return spot;
	}

	public void setSpot(Spot spot) {
		this.spot = spot;
	}

	public Lot getLot() {
		return lot;
	}

	public void setLot(Lot lot) {
		this.lot = lot;
	}

	public Integer getLotId() {
		return lotId;
	}

	public void setLotId(Integer lotId) {
		this.lotId = lotId;
	}

	public Date getStartTime() {
		return startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	public Date getEndTime() {
		return endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public Date getCreatedAt() {
		return createdAt;
	}


	public Date getUpdatedAt() {
		return updatedAt;
	}


	@Override
	public String toString() {
		return "Order{" +
				"id=" + id +
				", price=" + price +
				", startTime=" + startTime +
				", endTime=" + endTime +
				", status=" + status +
				", type=" + type +
				", createdAt=" + createdAt +
				", updatedAt=" + updatedAt +
				'}';
	}

	public String toJson()
	{
		return "{" +
				"id=" + id +
				", price=" + price +
				", startTime=" + startTime +
				", endTime=" + endTime +
				", status=" + status +
				", type=" + type +
				", createdAt=" + createdAt +
				", updatedAt=" + updatedAt +
				'}';
	}
}
