package com.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.models.enums.Type;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.search.annotations.IndexedEmbedded;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity(name = "transactions")
@DynamicUpdate
public class Transaction implements Serializable {
     @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Enumerated(EnumType.STRING)
    @Column(name = "type")
    private Type type;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "user_id", nullable = false)
	@JsonIgnore
	private User user;

//	@OneToOne(mappedBy = "transaction")
//    private Order order;

//    @OneToOne(cascade=CascadeType.ALL)
////    @MapsId
////    @ManyToOne(cascade=CascadeType.ALL)
////	@ManyToOne(fetch = FetchType.EAGER)
//    @JoinColumn(name = "order_id")
//    private Order order;
    @IndexedEmbedded
	@OneToOne(cascade=CascadeType.ALL)
	@JoinColumn(name = "order_id")
	private Order order;


    @Column(name = "amount")
    private Integer amount;

    @Column(name = "status")
    private Boolean status;

    @Column(name = "created_at", updatable = false, insertable=false)
    private Date createdAt;

    @Column(name = "updated_at", updatable = false, insertable = false)
    private Date updatedAt;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Order getOrder() {
        return order;
    }

    public void setOrder(Order order) {
        this.order = order;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }
    public void set(Type type, User user) {
        this.type = type;
        this.user = user;
    }
}
