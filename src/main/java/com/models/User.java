package com.models;

import javax.persistence.*;
import javax.validation.constraints.*;


import com.fasterxml.jackson.annotation.JsonIgnore;

import org.hibernate.annotations.DynamicUpdate;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity(name = "users")
@DynamicUpdate
public class User implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

//	@Pattern(regexp="^[0][1-9]\\d{9}$|^[1-9]\\d{9}$" ,message = "Incorrect mobile number")
	@Size(min=10, max=13) //@Pattern(regexp="[0-9]")
	private String mobile;

	@Size(min=5, max=50) @Pattern(regexp="[^0-9]*")
	private String name;

	@Column(name = "type", insertable=false)
	private Integer type;

	@Email
	private String email;

	@JsonIgnore
	private String password;

	@OneToMany(fetch = FetchType.EAGER, mappedBy = "user")
	@JsonIgnore
	private Set<Car> cars;

	@OneToMany(fetch = FetchType.EAGER, mappedBy = "user")
	@JsonIgnore
	private Set<Order> orders;

	@ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinTable(
        name = "user_lots",
        joinColumns = { @JoinColumn(name = "user_id") },
        inverseJoinColumns = { @JoinColumn(name = "lot_id") }
    )
	@JsonIgnore
    Set<Lot> lots = new HashSet<>();

	//	@Size(min = 5, max = 5)
	@Column(name = "confirm_mobile")
	private String confirmMobile;

	@Column(name = "status", insertable=false)
	private Boolean status;

	@Column(name = "created_at", updatable = false, insertable=false)
	private Date createdAt;

	@Column(name = "updated_at", updatable = false, insertable = false)
	private Date updatedAt;

	@Column(name = "token")
	@JsonIgnore
	private String token;

	@Column(name = "role")
	@JsonIgnore
	private String role;

	@Column(name = "remember_token")
	@JsonIgnore
	private String rememberToken;


//	@JsonIgnore
//	private Integer lot;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getConfirmMobile() {
		return confirmMobile;
	}

	public void setConfirmMobile(String confirmMobile) {
		this.confirmMobile = confirmMobile;
	}

	public Integer getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public Set<Car> getCars() {
		return cars;
	}

	public void setCars(Set<Car> cars) {
		this.cars = cars;
	}

	public Set<Order> getOrders() { return orders; }

	public void setOrders(Set<Order> orders) { this.orders = orders; }

	public Set<Lot> getLots() {
		return lots;
	}

	public void setLots(Set<Lot> lots) {
		this.lots = lots;
	}

	public Boolean getStatus() {
		return status;
	}

	public void setStatus(Boolean status) {
		this.status = status;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public Date getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}

//	public Integer getLot() {
//		return lot;
//	}
//
//	public void setLot(Integer lot) {
//		this.lot = lot;
//	}

	@Override
	public String toString() {
		return "User{" +
				"id=" + id +
				", name='" + name + '\'' +
				", mobile='" + mobile + '\'' +
				", token='" + token + '\'' +
				", confirmMobile=" + confirmMobile +
				", type=" + type +
				", email='" + email + '\'' +
				", password='" + password + '\'' +
				", status=" + status +
				", createdAt=" + createdAt +
				", updatedAt=" + updatedAt +
//				", lot=" + lot +
				'}';
	}
}


