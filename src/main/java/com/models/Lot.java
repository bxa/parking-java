package com.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.apache.lucene.analysis.core.LowerCaseFilterFactory;
import org.apache.lucene.analysis.core.WhitespaceTokenizerFactory;
import org.apache.lucene.analysis.ngram.EdgeNGramFilterFactory;
import org.apache.lucene.analysis.snowball.SnowballPorterFilterFactory;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.search.annotations.*;
import org.hibernate.search.annotations.Index;
import org.hibernate.search.annotations.Parameter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@AnalyzerDef(name = "namer", tokenizer = @TokenizerDef(factory = WhitespaceTokenizerFactory.class), filters = {
		@TokenFilterDef(factory = LowerCaseFilterFactory.class),
		@TokenFilterDef(factory = SnowballPorterFilterFactory.class, params = { @Parameter(name = "language", value = "English") }),
		@TokenFilterDef(factory = EdgeNGramFilterFactory.class, params = { @Parameter(name = "maxGramSize", value = "15") })

})

@Entity(name = "lots")
@DynamicUpdate
//@Indexed
@Access(AccessType.FIELD)
public class Lot  implements Serializable {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer  id;

    @Field(index=Index.YES, analyze=Analyze.YES, store=Store.NO, analyzer = @Analyzer(definition = "namer"))
    private String name;

//	@Column(name = "lat")
//	private Float lat;
//
//    @Column(name = "lng")
//	private Float lng;

	@OneToMany(fetch = FetchType.EAGER, mappedBy = "lot")
	@JsonIgnore
	private Set<Spot> spots;

	@Column(name = "status", insertable=false)
	private int status;

	@Column(insertable=false)
	private int type;

	private int capacity;

	private String phone;

    @IndexedEmbedded
	@OneToOne(cascade=CascadeType.ALL)
	@JoinColumn(name = "address_lot_id")
	private AddressLot addressLot;

	@OneToMany(mappedBy = "lot")
	@JsonIgnore
	private Set<Spot> lotSpots = new HashSet<Spot>(0);

	@ManyToMany(mappedBy = "lots")
    private Set<User> users = new HashSet<>();

	@Column(name = "created_at", updatable = false, insertable=false)
	private Date createdAt;

	@Column(name = "updated_at", updatable = false, insertable = false)
	private Date updatedAt;

//
//	public Float getLat() {
//		return lat;
//	}
//
//	public void setLat(Float lat) {
//		this.lat = lat;
//	}
//
//	public Float getLng() {
//		return lng;
//	}
//
//	public void setLng(Float lng) {
//		this.lng = lng;
//	}

	public Set<Spot> getLotSpots() {
		return lotSpots;
	}

	public void setLotSpots(Set<Spot> lotSpots) {
		this.lotSpots = lotSpots;
	}

	public AddressLot getAddressLot() {
		return addressLot;
	}

	public void setAddressLot(AddressLot addressLot) {
		this.addressLot = addressLot;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public int getCapacity() {
		return capacity;
	}

	public void setCapacity(int capacity) {
		this.capacity = capacity;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPhone() {
		return phone;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getId() {
		return id;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public Date getCreatedAt() { return createdAt; }

	public Date getUpdatedAt() {
		return updatedAt;
	}

	@Override
	public String toString() {
		return "Lot{" +
				"id=" + id +
				", status=" + status +
				", type=" + type +
				", capacity=" + capacity +
				", name='" + name + '\'' +
				", phone='" + phone + '\'' +
				", createdAt=" + createdAt +
				", updatedAt=" + updatedAt +
				'}';
	}

	public Lot() {

	}

}
