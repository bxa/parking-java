package com.models.enums;

public enum Type {
    topup, withdraw, payment,
    // report types
    report, lot, operator, user, order
}
