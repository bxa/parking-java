package com.models.enums;

public enum Status {
    // parking spot status:
    NULL, free, busy, broken, under_destruction, un_available
}
