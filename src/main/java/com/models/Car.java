package com.models;
import org.hibernate.annotations.*;
import javax.persistence.*;
import javax.persistence.Entity;
import javax.validation.constraints.Size;
import java.io.Serializable;

@Entity(name = "cars")
@DynamicUpdate
public class Car implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column(name = "status")
    private Boolean status;
    @Column(name = "plate_number", unique = true)
    private String plateNumber;
    @Column(name = "model_year", unique = true)
    private String modelYear;
    @Column(name = "brand", unique = true)
    private String brand;
    @Column(name = "color")
    @Size(min = 2)
    private String color;
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name="user_id")
    private User user;
    public int getId() {
        return id;
    }

    public Boolean getStatus() {
        return status;
    }
    public void setStatus(Boolean status) { this.status = status; }
    public String getModelYear() {
        return modelYear;
    }
    public void setModelYear(String modelYear) {
        this.modelYear = modelYear;
    }
    public String getBrand() {
        return brand;
    }
    public void setBrand(String brand) {
        this.brand = brand;
    }
    public String getPlateNumber() {
        return plateNumber;
    }
    public void setPlateNumber(String plateNumber) {
        this.plateNumber = plateNumber;
    }
    public void setColor(String color) {
        this.color = color;
    }
    public String getColor() {
        return color;
    }
    public void setUser(User user) {
        this.user = user;
    }
    public User getUser() {
        return user;
    }

    @Override
    public String toString() {
        return "Car{" +
                "id=" + id +
                ", plate_number=" + plateNumber+
                ", model_year=" + modelYear+
                ", brand=" + brand+
                ", color=" + color +
                '}';
    }
}
