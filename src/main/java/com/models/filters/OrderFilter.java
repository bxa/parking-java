package com.models.filters;
import com.fasterxml.jackson.annotation.JsonFilter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.models.Lot;
import com.models.Spot;
import com.models.User;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.util.Date;


@Entity(name = "orders")
@JsonFilter("orderFilter")
public class OrderFilter {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonProperty
	private Integer id;

	@JsonProperty
	private Integer price;


	@Column(name = "start_time")
	@JsonProperty
	private Date startTime;

	@Column(name = "end_time", insertable=false)
	@JsonProperty
	private Date endTime;

	@Column(insertable=false)
	@JsonProperty
	private Integer status;

	@Column(insertable=false)
	@JsonProperty
	private Integer type;

	@JoinColumn(name = "user_id")
	@ManyToOne(fetch = FetchType.EAGER)
	@JsonProperty
	private User user;

	@Column(name = "user_id", insertable = false, updatable = false)
	@JsonProperty
	private Integer userId;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "spot_id")
	@JsonProperty
	private Spot spot;

	@Column(name = "spot_id", insertable = false, updatable = false)
	@JsonProperty
	private Integer spotId;

	@JoinColumn(name = "lot_id")
	@ManyToOne(fetch = FetchType.EAGER)
	@JsonProperty
	private Lot lot;

	@Column(name = "lot_id", insertable = false, updatable = false)
	@JsonProperty
	private Integer lotId;

    @Column(name = "deleted", columnDefinition = "boolean default false")
	@JsonProperty
    private Boolean deleted = false;

	@Column(name = "created_at", updatable = false, insertable=false)
	@JsonProperty
	private java.util.Date createdAt;

	@Column(name = "updated_at", updatable = false, insertable = false)
	@JsonProperty
	private java.util.Date updatedAt;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getPrice() {
		return price;
	}

	public void setPrice(Integer price) {
		this.price = price;
	}

	public Date getStartTime() {
		return startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	public Date getEndTime() {
		return endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public Spot getSpot() {
		return spot;
	}

	public void setSpot(Spot spot) {
		this.spot = spot;
	}

	public Integer getSpotId() {
		return spotId;
	}

	public void setSpotId(Integer spotId) {
		this.spotId = spotId;
	}

	public Lot getLot() {
		return lot;
	}

	public void setLot(Lot lot) {
		this.lot = lot;
	}

	public Integer getLotId() {
		return lotId;
	}

	public void setLotId(Integer lotId) {
		this.lotId = lotId;
	}

	public Boolean getDeleted() {
		return deleted;
	}

	public void setDeleted(Boolean deleted) {
		this.deleted = deleted;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public Date getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}
}

