package com.models;

import javax.persistence.*;


import com.fasterxml.jackson.annotation.JsonIgnore;

import com.models.enums.Status;
import org.hibernate.annotations.DynamicUpdate;

import java.io.Serializable;
import java.util.Set;

@Entity(name = "spots")
@DynamicUpdate
@Access(AccessType.FIELD)
public class Spot  implements Serializable {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "spot")
    @JsonIgnore
    private Set<Order> orders;


    @Column(name = "lora_serial")
	private Integer loraSerial;

    @ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "lot_id", nullable = false)
    @JsonIgnore
	private Lot lot;

    @Enumerated(EnumType.STRING)
    private Status status;

	@Column(insertable=false)
	private Integer type;

	@Column(name = "created_at", updatable = false, insertable=false)
	private java.util.Date createdAt;

	@Column(name = "updated_at", updatable = false, insertable = false)
	private java.util.Date updatedAt;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}


    public Set<Order> getOrders() {
        return orders;
    }

    public void setOrders(Set<Order> orders) {
        this.orders = orders;
    }

	public Integer getLoraSerial() {
		return loraSerial;
	}

	public void setLoraSerial(Integer loraSerial) {
		this.loraSerial = loraSerial;
	}

	public Lot getLot() {
		return lot;
	}

	public void setLot(Lot lot) {
		this.lot = lot;
	}


//	@Enumerated(EnumType.STRING)
	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public java.util.Date getCreatedAt() {
		return createdAt;
	}

	public java.util.Date getUpdatedAt() {
		return updatedAt;
	}

	@Override
	public String toString() {
		return "Spot{" +
				"id=" + id +
				", status=" + Status.valueOf(status.toString()) +
				", type=" + type +
				", loraSerial=" + loraSerial +
				", createdAt=" + createdAt +
				", updatedAt=" + updatedAt +
				'}';
	}
	//	private AddressSpot spotAddress;
}
