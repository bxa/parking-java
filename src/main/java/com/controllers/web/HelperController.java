package com.controllers.web;

import org.springframework.security.crypto.bcrypt.BCrypt;

import java.util.HashMap;
import java.util.Random;

public class HelperController {
    /**
     * Get a 5 digit confirmation code between 10,000 and 99,999
     * @return Integer
     */
    public Integer generateConfirmationCode(){
        Random rand = new Random();
        int code = rand.nextInt(89999);
        code += 10000;
		code = 11111;
        return code;
    }

    /**
     * Generate a hash from a integer
     * @param code
     * @return Hash
     */
    public String generateHashFromInt(Integer code) {
        String code1 = String.valueOf(code);
        String hashCode = BCrypt.hashpw(code1, BCrypt.gensalt());
        return hashCode;
    }

    /**
     * Send sms message
     * @return Boolean
     */
    public Boolean sendConfirmationCodeSMS(Integer code) {
        return true;
    }

    public int getPage(String page1) {
        int page = page1.isEmpty() || page1 == null || Integer.valueOf(page1) == 0 ? 0 : Integer.valueOf(page1);
        return page;
    }

    public HashMap assArray(String[] keys, Object[] values) {
        HashMap hm = new HashMap();
        for (int i = 0; i < keys.length; i++) {
            hm.put(keys[i], values[i]);
        }
        return hm;
    }

    public HashMap assArray(String key, Object value) {
        HashMap hm = new HashMap();
        hm.put(key, value);
        return hm;
    }

    public HashMap[] assArray(String key, String value) {
        HashMap hm;
        hm = new HashMap();
        hm.put(key, value);
        return new HashMap[]{hm};
    }


}
