package com.controllers.web.web;

import com.controllers.helpers.Responses;

import javax.servlet.http.HttpServletResponse;

public class WebResponse extends Responses {

	private String message;
	private Object metadata;
	private Object result;
	private Object errors;
	private HttpServletResponse res;
	private boolean status;
	private String redirect;

	//	}
	//		super();
	//	public ApiResponse() {
	public WebResponse(HttpServletResponse res) {
		super(res);
		this.res = res;
//		this.clear();
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	public String getMsg() {
		return message;
	}

	public void setMsg(String message) {
		this.message = message;
	}

	public String getRedirect() {
		return redirect;
	}

	public void setRedirect(String redirect) {
		this.redirect = redirect;
	}

	public Object getResult() {
		return result;
	}

	public void setResult(Object result) {
		this.result = result;
	}
}
