package com.controllers.web.web;
import com.controllers.helpers.Responses;
import com.controllers.web.BaseController;
import com.models.Order;
import com.models.User;
import com.services.LotService;
import com.services.OrderService;
import com.services.SpotService;
import com.services.UserService;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

@Controller("DashboardControllerWeb")
public class DashboardController extends BaseController {

    private LotService lotService;
    private OrderService orderService;
    private UserService userService;
    private SpotService spotService;

    @Autowired
    public DashboardController(HttpServletResponse res, SessionFactory sessionFactory, LotService lotService, SpotService spotService, OrderService orderService, UserService userService) {
        super(sessionFactory);

        this.userService = userService;
        this.lotService = lotService;
        this.orderService = orderService;
        this.spotService = spotService;

        this.wr = new WebResponse(res);
    }


    @RequestMapping("/dashboard/parking-history")
    public ModelAndView showParkHistory() {
        ModelAndView model = new ModelAndView("/dashboard/park-history");
        model.addObject("orders", orderService.get("WHERE user_id  = " + authUser().getId()));
        return model;
    }

//    @RequestMapping("/dashboard/park-history/json")
//    public  @ResponseBody
//    List<Order> jsonParkHistory() {
//        return orderService.get("WHERE user_id  = " + authUser().getId());
//    }

    @RequestMapping("/dashboard/lots-history")
    public ModelAndView showLotsHistory() {
        ModelAndView model = new ModelAndView("/dashboard/lots-history");
        return model;
    }

    @RequestMapping("/dashboard/payments-history")
    public ModelAndView showPaymentsHistory() {
        ModelAndView model = new ModelAndView("/dashboard/payment-history");
        return model;
    }
}
