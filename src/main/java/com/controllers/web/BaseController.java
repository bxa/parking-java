package com.controllers.web;


import com.controllers.web.web.WebResponse;
import com.dao.UserDao;
import com.models.User;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;

@Controller
public class BaseController extends HelperController {

    @Value("${searchLot.distance}")
    public String distance;
    public WebResponse wr;
    public Authentication authUser;
    //    @Autowired
    //    public HttpServletRequest req;
    public HttpServletResponse res;
    public String test;
    public HelperController helper;
    public final SessionFactory sessionFactory;
    public UserDao userDao;
    Session session;


    @Autowired
    public BaseController(SessionFactory sessionFactory)
    {
        super();
        this.sessionFactory = sessionFactory;
        this.helper = new HelperController();

    }

    @Transactional
    public User authUser()
    {
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        if (principal instanceof UserDetails) {
            Session session = sessionFactory.openSession();
//            String username = ((UserDetails)principal).getUsername();
            return (User) session.createQuery("from users where mobile="+ ((UserDetails) principal).getUsername()).list().get(0);
        } else {
            return new User();
        }
    }


    public void out(Object s) { System.out.println(s); }

}
