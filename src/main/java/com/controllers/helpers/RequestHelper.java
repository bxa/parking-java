package com.controllers.helpers;

import com.fasterxml.jackson.databind.ObjectMapper;
import jdk.internal.util.xml.impl.Input;
import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class RequestHelper {
    private HashMap params;

    public RequestHelper(HttpServletRequest req)
    {
        this.setParams(req);
    }

    private void setParams(HttpServletRequest req)
    {
        HashMap params = new HashMap();
        StringBuffer jb = new StringBuffer();
        String line = null;
        try {
            BufferedReader reader = req.getReader();
            while ((line = reader.readLine()) != null)
                jb.append(line);
        } catch (Exception e) { /*report an error*/ }
        ObjectMapper mapper = new ObjectMapper();
        HashMap jsonParams = new HashMap();
        try {
            jsonParams = mapper.readValue(jb.toString(), HashMap.class);
            System.out.println(jsonParams);
        } catch (IOException e) {
            e.printStackTrace();
        }

        Map params1 = req.getParameterMap();
        Iterator i = params1.keySet().iterator();
        while ( i.hasNext() )
        {
            String key = (String) i.next();
            String value = ((String[]) params1.get(key))[0];
            params.put(key, String.valueOf(value));
        }
        HashMap allParams = new HashMap();
        allParams.putAll(params);
        allParams.putAll(jsonParams);

        System.out.println("this is all params: ");
        System.out.println(allParams);
        this.params = allParams;
    }
    public HashMap getAllParams()
    {
        return this.params;
    }
    public String  get(String key)
    {

        if(params.get(key) != null) {
            return (String)params.get(key);
        }
        return "";
    }
    public Integer getInt(String key)
    {
        return Integer.valueOf(this.get(key));
    }
    public boolean notEmpty(String key)
    {
        if(params.get(key) != null) {
            return true;
        }
        return false;
    }
    public boolean empty(String key)
    {
        if(params.get(key) != null) {
            return false;
        }
        return true;
    }

}
