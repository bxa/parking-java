package com.controllers.helpers;

import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;

import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.List;

public class Responses {
    public String message;
	public Object metadata;
	public Object result;
	public Object errors;
	public HttpServletResponse res;

	public Responses(HttpServletResponse res) {
		super();
		this.res = res;
		this.clear();
	}
	public void clear()
	{
		this.message = null;
		this.result = null;
		this.metadata = null;
		this.errors = null;
	}
	public Responses success(String message) {
		this.clear();

		res.setStatus(200);
		this.message = message;
		return this;
	}
	public Responses success() {
		this.clear();

		res.setStatus(200);
		this.message = "Operation successfully done";
		return this;
	}
	public Responses created() {
		this.clear();

		res.setStatus(201);
		this.message = "Item created successfully";
		return this;
	}
	public Responses success(String message, Object result) {
		this.clear();

		res.setStatus(200);
		this.message = message;
		this.result = result;
		return this;
	}

	public Responses success(Object result) {
		this.clear();

		res.setStatus(200);
		this.message = "Success";
		this.result = result;
		return this;
	}

	public Responses success(Object result, Object metadata) {
		this.clear();

		res.setStatus(200);
		this.message = "Success";
		this.result = result;
		this.metadata = metadata;
		return this;
	}

	public Responses success(HashMap[] result) {
		this.clear();

		res.setStatus(200);
		this.message = "Success";
		this.result = result;
		return this;
	}


//	public Responses success(HashMap[] result) {
//		this.clear();
//
//		res.setStatus(200);
//		this.message = "Success";
//		this.result = result;
//		return this;
//	}

	public Responses success(String message, Object result, Object metadata) {
		this.clear();

		res.setStatus(200);
		this.message = message;
		this.result = result;
		this.metadata = metadata;
		return this;
	}

	public Responses errorFormMessage(String message) {
		this.clear();

		res.setStatus(400);
		this.message = message;
		return this;
	}
	public Responses formError(Object errors, String message) {
		this.clear();

		res.setStatus(400);
		this.message = message;
		this.errors = errors;
		return this;
	}
	public Responses notFoundError(String message) {
		this.clear();

		res.setStatus(404);
		this.message = message;
		return this;
	}

	public Responses formError(BindingResult errors) {
		this.clear();

		HashMap err = this.parseErrors(errors);
		res.setStatus(400);
		this.message = "There is a form error";
		this.errors = err;
		return this;

	}

	public Responses formError(HashMap[] errors) {
		this.clear();

		res.setStatus(400);
		this.message = "There is a form error";
		this.errors = errors;
		return this;
	}

	public Responses serverError(String message) {
		this.clear();

		res.setStatus(500);
		this.message = message;
		return this;
	}

	public Responses serverError() {
		this.clear();

		res.setStatus(500);
		this.message = "There is an error with your transaction";
		return this;
	}
	public Responses tst(String message) {
		this.clear();

		res.setStatus(500);
		this.message = message;
		return this;
	}

	public Responses unAuthorized() {
		this.clear();

		res.setStatus(401);
		this.message = "you don't have access to this item";
		return this;
	}

	public HashMap parseErrors(BindingResult errors)
	{
		List<FieldError> errors1 = errors.getFieldErrors();

		HashMap errs = new HashMap();
		for (FieldError error : errors1 ) {
			errs.put(error.getField(), error.getDefaultMessage());
//			errs.put(error.getObjectName(), error.getDefaultMessage());
//			errs.put(error., error.getDefaultMessage());
			System.out.println (error.getObjectName() + " - " + error.getDefaultMessage());
		}
		return errs;
	}



	public void setErrors(Object errors) { this.errors = errors; }

	public void successMessage(String message) {
		this.message = message;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Object getMetadata() {
		return metadata;
	}

	public void setMetadata(String metadata) {
		this.metadata = metadata;
	}

	public Object getErrors() { return errors; }

	public Object getResult() {
		return result;
	}

	public void setResult(Object result) {
		this.result = result;
	}
}
