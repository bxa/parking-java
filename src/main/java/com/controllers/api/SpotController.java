package com.controllers.api;

import com.controllers.api.constants.Routes;
import com.controllers.helpers.Responses;
import com.dao.LotDao;
import com.dao.OrderDao;
import com.dao.SpotDao;
import com.dao.UserDao;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ser.impl.SimpleBeanPropertyFilter;
import com.fasterxml.jackson.databind.ser.impl.SimpleFilterProvider;

import com.models.Lot;
import com.models.Order;
import com.models.Spot;
import com.models.enums.Status;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@RestController("SpotControllerApi")
//@ComponentScan({"com.jfilter.components"})
//@EnableJsonFilter
public class SpotController extends BaseController {


    private final UserDao userDao;
    private final LotDao lotDao;
    private final SpotDao spotDao;
    public OrderDao orderDao;
    HttpServletRequest req;
//    private final SessionFactory sessionFactory;


    @Autowired
    public SpotController(HttpServletResponse res, HttpServletRequest req, LotDao lotDao, OrderDao orderDao, SpotDao spotDao, UserDao userDao, SessionFactory sessionFactory)
    {
        super(userDao, sessionFactory);

        this.orderDao = orderDao;
        this.userDao = userDao;
        this.lotDao = lotDao;
        this.spotDao = spotDao;
        this.req = req;
        this.ar = new ApiResponse(res);
    }

    /**
     * get a lot by id
     * @return
     */
    @GetMapping("/lots/{id}/spots")
    public @ResponseBody
    Responses lotSpots(@PathVariable @NotNull @DecimalMin("0") String id, HttpServletRequest req)
    {
        int lotId = new Integer(id);
        if (!(lotId > 0)) {
            return ar.formError(assArray("id", "Id is not valid"));
        }
        Set<Spot> lotSpots = lotDao.get(lotId).getLotSpots();
        if (lotSpots.size() > 0) {
            String page = getParam(req, "page");
            Integer counts = spotDao.counts("where lot_id=" + lotId);
            HashMap meta = createMetadata(counts, page, itemsPerPage);
            return ar.success(lotSpots, meta);
        }
        return ar.notFoundError("Lot not found or it doesn't have spot");
    }

    /** operator section */

    /**
     * get a lot spots by id
     * @return
     */
    @GetMapping(Routes.operatorRoute + "/lots/{id}/spots")
    @Transactional
    public @ResponseBody
    Responses operatorLotSpots(@PathVariable @NotNull @DecimalMin("0") String id, HttpServletRequest req)
    {
//        authUser(req);
        int lotId = new Integer(id);
        if (!(lotId > 0)) {
            return ar.formError(assArray("id", "Id is not valid"));
        }

        authUser(req);
        Set<Lot> userLots = authUser.getLots();
        Lot lot = lotDao.get(lotId);

        if(!userLots.contains(lot)) {
            return ar.unAuthorized();
        }

        Set<Spot> lotSpots = lot.getLotSpots();
        if (lotSpots.size() > 0) {
            return ar.success(lotSpots);
        }
        return ar.notFoundError("Lot not found");
    }

    /**
     * get a lot spot
     * @param id
     * @param spot_id
     * @param req
     * @return
     */
    @GetMapping(Routes.operatorRoute + "/lots/{id}/spots/{spot_id}")
    @Transactional
    public @ResponseBody Responses operatorLotSpot(@PathVariable @NotNull @DecimalMin("0") String id, @PathVariable @NotNull @DecimalMin("0") String spot_id, HttpServletRequest req)
    {
        int lotId = new Integer(id);
        if (!(lotId > 0)) {
            return ar.formError(assArray("id", "Id is not valid"));
        }
        int spotId = new Integer(spot_id);
        if (!(spotId > 0)) {
            return ar.formError(assArray("id", "Spot id is not valid"));
        }

        authUser(req);
        Lot lot = lotDao.get(lotId);
        Set<Lot> userLots = authUser.getLots();
        if(!userLots.contains(lot)) {
            return ar.unAuthorized();
        }

        Spot spot = spotDao.get(spotId);
        Set<Spot> spots = lot.getLotSpots();
        if(!spots.contains(spot)) {
            return ar.notFoundError("There is no such spot in this lot");
        }

        return ar.success(spot);
    }

    /**
     * get spot history of orders
     * @param req
     * @return
     */
    @GetMapping(Routes.operatorRoute + "/eventshg")
    @Transactional
//    @JsonIgnoreProperties({"lot","spot"})
    public @ResponseBody Responses operatorLotSpotEvents(HttpServletRequest req)
    {

        String page = getParam(req, "page");
        String spotId = getParam(req, "spotId");
        String lotId = getParam(req, "lotId");
        out("[SpotController:operatorLotSpotEvents]:");
        out(req);
        out(spotId.isEmpty());
        out(lotId.isEmpty());


        authUser(req);

//        List<Order> allOrders = null;
        Set<Lot> userLots = authUser.getLots();
//        if(spotId.isEmpty() && lotId.isEmpty()) {
//            for (Lot lot: userLots) {
//                out("Lot id:"+ lot.getId());
//                allOrders = orderDao.get("where lot_id=33", page).list();
////                allOrders = q.list();
////                allOrders.addAll(q.list());
//
//                out(allOrders);
//            }
//        }
//        Set<Lot> userLots = authUser.getLots();
        Lot lot = lotDao.get(new Integer(lotId));
        if(!userLots.contains(lot)) {
            return ar.unAuthorized();
        }
//
//        Spot spot = spotDao.get(new Integer(spotId));
//        Set<Spot> spots = lot.getLotSpots();
//        if(!spots.contains(spot)) {
//            return ar.notFoundError("There is no such spot in this lot");
//        }
        Query q = orderDao.get("where lot_id="+lot.getId(), page);
        List<Order>  spotOrders = q.list();
//
//        Integer orderCounts = orderDao.counts("where lot_id=" + lot.getId());
//        HashMap meta = createMetadata(orderCounts, page, itemsPerPage);


        // filter some fields
//        out(spotOrders);
//        SimpleFilterProvider filterProvider = new SimpleFilterProvider();
//        filterProvider.addFilter("orderFilter",
//                SimpleBeanPropertyFilter.serializeAllExcept("spot","user","lot"));
//
//        ObjectMapper mapper = new ObjectMapper();
//        mapper.setFilterProvider(filterProvider);
//        String jsonData = "";
//        Set orders = new HashSet<>();
//        try {
//            jsonData = mapper.writerWithDefaultPrettyPrinter()
//                    .writeValueAsString(spotOrders);
//            orders = mapper.readValue(jsonData, new TypeReference<Set>() {});
//            out(orders);
//        } catch (Exception e) {
//            ar.serverError();
//        }

        return ar.success(spotOrders);
    }

    @PutMapping(Routes.operatorRoute + "/spots/{id}/status")
    @Transactional
    public @ResponseBody Responses toggleStatus(@PathVariable @NotNull @DecimalMin("0") String id,  @RequestParam Status status, HttpServletRequest req)
    {
        int spotId = new Integer(id);
        if (!(spotId > 0)) {
            return ar.formError(assArray("id", "Spot id is not valid"));
        }
        authUser(req);
        Spot spot = spotDao.get(spotId);
        Lot lot = spot.getLot();
        if(lot != null) {
            Set<Lot> userLots = authUser.getLots();
            if(!userLots.contains(lot)) {
                return ar.unAuthorized();
            }
            spot.setStatus(status);

            boolean updated = spotDao.save(spot, spot);
            if (updated) {
                return ar.success("Updated successfully", spot);
            }
        } else {
            return ar.notFoundError("Lot not found");
        }
        return ar.serverError();
    }
}
