package com.controllers.api.admin;

import org.springframework.beans.factory.annotation.Value;

import javax.servlet.http.HttpServletRequest;
import java.util.*;

public class HelperController extends com.controllers.web.HelperController {
    @Value("${itemsPerPage}")
    public int itemsPerPage;

    public HelperController() {
    }


    public HashMap createMetadata(Integer totalItems, Object page, Integer itemsPerPage) {
        int page1 = getPage(String.valueOf(page));
        HashMap meta = assArray(
                new String[]{"totalItems", "page", "totalPage", "size"},
                new Object[]{totalItems, page1, totalItems / Integer.valueOf(itemsPerPage), Integer.valueOf(itemsPerPage)}
        );
        return meta;
    }

    public String getParam(HttpServletRequest req, String key)
    {
        return req.getParameter(key) != null ? req.getParameter(key) : "";
    }

    public HashMap getParams(HttpServletRequest req)
    {
        HashMap params = new HashMap();
        Map params1 = req.getParameterMap();
        Iterator i = params1.keySet().iterator();
        while ( i.hasNext() )
        {
            String key = (String) i.next();
            String value = ((String[]) params1.get(key))[0];
            params.put(key, String.valueOf(value));
        }
        return params;
    }

    public void out(Object s) {
        System.out.println(s);
    }

    public boolean validateLatLng(String lat, String lng)
    {
        if(lat.isEmpty() || lng.isEmpty()) {
            return false;
        }
        Float lat1 = Float.valueOf(lat);
        Float lng1 = Float.valueOf(lng);
        if((lat1 < -90 || lat1 > 90)  || (lng1 < -180 || lng1 > 180)) {
            return false;
        }
        return true;
    }
}
