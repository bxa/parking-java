package com.controllers.api.admin;

import com.controllers.api.admin.ApiResponse;
import com.controllers.api.admin.BaseController;
import com.controllers.api.constants.Routes;
import com.controllers.helpers.Responses;
import com.dao.CarDao;
import com.dao.UserDao;
import com.models.Car;
import com.models.User;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.List;

/**
 * @author Behrooz
 *
 */
@RestController("UserControllerAdmin")

public class UserController extends BaseController {

    private final UserDao userDao;
    private final CarDao carDao;
    HttpServletRequest req;

    @Autowired
    public UserController(HttpServletResponse res, HttpServletRequest req, UserDao userDao, CarDao carDao, SessionFactory sessionFactory)
    {
        super(userDao, sessionFactory);
        this.userDao = userDao;
        this.carDao= carDao;
        this.req = req;
        this.ar = new ApiResponse(res);
//        this.req = req;
    }
    /**
     * Check token and if it is ok return the user
     */
    @GetMapping("/checkToken")
    public @ResponseBody
    Responses checkToken(HttpServletRequest req)
    {
        authUser(req);

        HashMap res = assArray("user", authUser);
        return ar.success(res);
    }



    /**
     * ADMIN section
     * */

    @GetMapping(Routes.adminRoute + "/users")
    @Transactional
    public @ResponseBody Responses users(HttpServletRequest req)
    {
        authUser(req);

//        out(authUser);
        String page = getParam(req, "page");
//        String queryString = " where plate_number=:plate";

        Query query = userDao.get("", page);
//        query.setParameter("plate", newUser.getPlateNumber());

        List<User> users = query.list();
        if(users.size() > 0) {
            Integer userCounts = userDao.counts("");

            HashMap meta = createMetadata(userCounts, page, itemsPerPage);
            return ar.success(users, meta);
        }
        return ar.notFoundError("There is No user yet");
    }
}
