package com.controllers.api;

import com.controllers.helpers.Responses;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;

import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.List;

public class ApiResponse extends Responses {

	private String message;
	private Object metadata;
	private Object result;
	private Object errors;
	private HttpServletResponse res;

	//	}
	//		super();
	//	public ApiResponse() {
	public ApiResponse(HttpServletResponse res) {
		super(res);
		this.res = res;
//		this.clear();
	}



//	@Override
//	public Responses success(Object result) {
//		this.clear();
//
//		res.setStatus(210);
//		this.message = "Success";
//		this.result = result;
//		return this;
//	}

}
