package com.controllers.api;

import com.controllers.api.constants.Routes;
import com.controllers.helpers.Responses;
import com.dao.CarDao;
import com.dao.LotDao;
import com.dao.ModelDao;
import com.dao.UserDao;
import com.models.Lot;
import com.models.Order;
import com.models.User;
import com.services.LotService;
import com.services.OrderService;
import com.services.UserService;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@RestController("DashboardControllerApi")
//@RequestMapping("/api/v.1")
public class DashboardController extends BaseController {

    private final UserDao userDao;
    private final CarDao carDao;
    private final LotDao lotDao;
    private final ModelDao modelDao;
    HttpServletRequest req;

    public DashboardController(HttpServletResponse res, HttpServletRequest req, UserDao userDao, ModelDao modelDao, LotDao lotDao, CarDao carDao, SessionFactory sessionFactory)
    {
        super(userDao, sessionFactory);
        this.userDao = userDao;
        this.carDao= carDao;
        this.modelDao= modelDao;
        this.lotDao= lotDao;
        this.req = req;
        this.ar = new ApiResponse(res);
    }

    /**
     * get dashboard statistics
     * @param req
     * @return
     */
    @GetMapping(Routes.operatorRoute + "/statistics")
    @Transactional
    public @ResponseBody
    Responses statistics(HttpServletRequest req)
    {
        authUser(req);
        Set<Lot> userLots = authUser.getLots();
        String lotIds = userLots.stream().map(pLot -> String.valueOf(pLot.getId())).collect(Collectors.joining(","));

        String queryString = "select count(*) from lots where id in("+lotIds+")";
        List lots = modelDao.countsQuery(queryString, "").list();
        Object countLots = lots.get(0);

        queryString = "select count(*) from spots where lot_id in("+lotIds+")";
        List counts = modelDao.countsQuery(queryString, "").list();
        Object countSpots = counts.get(0);

        queryString = "select count(*) from spots where status='busy' and lot_id in("+lotIds+")";
        counts = modelDao.countsQuery(queryString, "").list();
        Object countBusySpots = counts.get(0);

        queryString = "select count(*) from spots where status='free' and lot_id in("+lotIds+")";
        counts = modelDao.countsQuery(queryString, "").list();
        Object countFreeSpots = counts.get(0);

        queryString = "select count(*) from spots where status <> 'free' and status <> 'busy' and lot_id in("+lotIds+")";
        counts = modelDao.countsQuery(queryString, "").list();
        Object countOtherSpots = counts.get(0);

        queryString = "select count(*) from orders where lot_id in("+lotIds+")";
        counts = modelDao.countsQuery(queryString, "").list();
        Object countOrders = counts.get(0);

        queryString = "select sum(price) from orders where lot_id in("+lotIds+")";
        counts = modelDao.countsQuery(queryString, "").list();
        Object ordersTotalPrice = counts.get(0);

        queryString = "select count(*) from reports where type='lot' and item_id in("+lotIds+")";
        counts = modelDao.countsQuery(queryString, "").list();
        Object countReport = counts.get(0);

        String[] keys = new String[]{
            "countLots",
            "countSpots",
            "countBusySpots",
            "countFreeSpots",
            "countOtherSpots",
            "countOrders",
            "ordersTotalPrice",
            "countReport",
        };
        Object[] values = new Object[]{
            countLots,
            countSpots,
            countBusySpots,
            countFreeSpots,
            countOtherSpots,
            countOrders,
            ordersTotalPrice,
            countReport,
        };
        HashMap res = assArray(keys, values);

        return ar.success(res);
    }

    @GetMapping(Routes.operatorRoute + "/checkForUpdate")
    @Transactional
    public @ResponseBody Responses checkForUpdate(HttpServletRequest req)
    {
        authUser(req);
        return ar.success("You are up to date");
    }

}
