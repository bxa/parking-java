package com.controllers.api;

import com.dao.UserDao;
import com.models.User;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

@RestController("BaseControllerApi")
@RequestMapping("/api/v.1")
@Transactional
public class BaseController extends HelperController {

    @Value("${searchLot.distance}")
    public String distance;
    public ApiResponse ar;
    public User authUser;
    public final UserDao userDao;
    //    @Autowired
    //    public HttpServletRequest req;
    public HttpServletResponse res;
    public String test;
    public HelperController helper;
    public final SessionFactory sessionFactory;
    Session session;

    @Autowired
    public BaseController(UserDao userDao, SessionFactory sessionFactory)
    {
        super();
        this.userDao = userDao;
        this.sessionFactory = sessionFactory;
        this.helper = new HelperController();

        this.ar = new ApiResponse(res);
    }

    public void authUser(HttpServletRequest req)
    {
        User user = new User();
        String token = req.getHeader("authorization");
        if(token != null) {
            List<User> users = userDao.getBy("token", token);
            if (users != null && !users.isEmpty()) {
                user = users.get(0);
                if (user.getId() > 0) {
                    user = user;
                }
            }
        }
        this.authUser = user;
    }
}
