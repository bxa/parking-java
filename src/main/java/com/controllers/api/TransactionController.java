package com.controllers.api;

import com.controllers.api.constants.Routes;
import com.controllers.helpers.Responses;
import com.dao.LotDao;
import com.dao.TransactionDao;
import com.dao.UserDao;
import com.models.Lot;
import com.models.Transaction;
import com.models.enums.Type;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@RestController("TransactionControllerApi")
public class TransactionController extends BaseController {

    private final UserDao userDao;
    private final LotDao lotDao;
    private final TransactionDao transactionDao;
    HttpServletRequest req;
//    private final SessionFactory sessionFactory;



    @Autowired
    public TransactionController(HttpServletResponse res, HttpServletRequest req, LotDao lotDao, UserDao userDao, TransactionDao transactionDao, SessionFactory sessionFactory)
    {
        super(userDao, sessionFactory);

        this.userDao = userDao;
        this.lotDao = lotDao;
        this.transactionDao= transactionDao;
        this.req = req;
        this.ar = new ApiResponse(res);
    }

//    /**
//     * topup
//     * @param transaction
//     * @param req
//     * @return
//     */
//    @PostMapping(Routes.profileRoute + "/topup")
//    @Transactional
//    public @ResponseBody
//    Responses topup(@RequestBody Transaction transaction, HttpServletRequest req)
//    {
//        Integer amount = transaction.getAmount();
//        out("this is amount: " + amount);
//        if (!(amount >= 5)) {
//            return ar.formError(assArray("amount", "Amount should be more that 5$"));
//        }
//        authUser(req);
//
//        try {
//            transaction.set(Type.topup, authUser);
//            transactionDao.save(transaction);
//            authUser.setBalance(authUser.getBalance() + amount);
//            userDao.save(authUser, authUser);
//        } catch (Exception e) {
//            return ar.serverError();
//        }
//
//        if(transaction != null && transaction.getId() > 0) {
//            return ar.success("Successful Transaction");
//        }
//        return ar.serverError();
//    }

    /**
     * get a user transactions
     * @param req
     * @return
     */
    @GetMapping(Routes.profileRoute + "/transactions")
    @Transactional
    public @ResponseBody
    Responses transactions(HttpServletRequest req)
    {
        authUser(req);

        String page = getParam(req, "page");
        Query qtransactions = transactionDao.get(" where user_id=" + authUser.getId(), page);

        List<Transaction> transactions = qtransactions.list();

        Integer transactionCounts = transactionDao.counts(" where user_id=" + authUser.getId());

        HashMap meta = createMetadata(transactionCounts, page, itemsPerPage);

        return ar.success(transactions, meta);
    }


    /** Operator methods */

    /**
     * get operator transaction on lots
     * @param req
     * @return
     */
    @GetMapping(Routes.operatorRoute + "/transactions")
    @Transactional
    public @ResponseBody
    Responses operatorTransactions(HttpServletRequest req)
    {
        authUser(req);
        String page = getParam(req, "page");

        Set<Lot> userLots = authUser.getLots();
        String lotIds = userLots.stream().map(pLot -> String.valueOf(pLot.getId())).collect(Collectors.joining(","));

        String where = " in (" + lotIds + ")";
        if(req.getParameter("lot_id") != null) {
            where = "="+req.getParameter("lot_id");
        }
        String queryString = "select * from transactions where exists (select * from orders where transactions.order_id=orders.id and lot_id" + where + ") and type='payment' order by id desc";


        Query qTransactions = (Query) transactionDao.getQuery(queryString, page);
        List<Transaction> transactions = qTransactions.list();

        Query qCount = transactionDao.countsQuery(queryString);
        int transactionCounts =  qCount.getResultList().size();

        HashMap meta = createMetadata(transactionCounts, page, itemsPerPage);

        return ar.success(transactions, meta);
    }



}
