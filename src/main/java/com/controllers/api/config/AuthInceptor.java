package com.controllers.api.config;
import com.dao.UserDao;
import com.models.User;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Arrays;
import java.util.List;

public class AuthInceptor implements HandlerInterceptor {

//    String[] homes;
    private String[] justAuth;
    private String[] driver;
    private String[] operator;
    private String[] admin;
    private String[] roles;
    private String[] unauthorize;
    public final UserDao userDao;


    public AuthInceptor(UserDao userDao)
    {
        this.justAuth = new String[]{"/profile", "/events"};
        this.driver = new String[]{"/user"};
        this.operator = new String[]{"/operator"};
        this.admin = new String[]{"/admin"};
//        this.roles = new String[];
//        this.roles["driver"] = "driver";

        this.unauthorize = new String[]{"/","/home","/login","/code","/checkRes","/test","/lots","/checkToken"};
        this.userDao = userDao;
//        boolean contains = Arrays.stream(values).anyMatch("s"::equals);
    }

    @Override
    public boolean preHandle(HttpServletRequest request,
                                  HttpServletResponse response, Object handler) throws Exception {
//        System.out.println(request.getMethod());
        System.out.println("[AtuhInceptor:preHandle]");
        if(!request.getRequestURI().contains("/api/v.1") || request.getMethod().equalsIgnoreCase("OPTIONS")) {
            return true;
        }
        String urlParam = request.getRequestURI().replace("/api/v.1","");
        String[] params = urlParam.split("/");
        if(params.length == 0 || params[1] == null) {
            System.out.println("header");
            return true;
        }
//        System.out.println(params);
//        System.out.println(urlParam);
        urlParam = "/" + params[1];
//        System.out.println(urlParam + " this is new");
        // check if url do not need authentication so pass user
        boolean containsUnAuthorize = Arrays.asList(this.unauthorize).contains(urlParam);
        if(containsUnAuthorize) {
            System.out.println("contains unAuthorize");
            return true;
        }

        // check if this url need authentication
        boolean containsJustAuth = Arrays.asList(this.justAuth).contains(urlParam);
        boolean containsDriver = Arrays.asList(this.driver).contains(urlParam);
        boolean containsOperator = Arrays.asList(this.operator).contains(urlParam);
        boolean containsAdmin = Arrays.asList(this.admin).contains(urlParam);
        if (containsDriver || containsOperator || containsJustAuth || containsAdmin) {
            String token = request.getHeader("authorization");
            if(token != null) {
                List<User> users = userDao.getBy("token", token);
                System.out.println(token);
                if (users != null && !users.isEmpty()) {
                    User user = users.get(0);
                    if(user.getId() > 0) {
//                        System.out.println(user.getId() + " user found");
                        if(containsJustAuth) {
                            System.out.println(user.getId() + " this needs just auth");
                            return true;
                        } else if(containsDriver) {
                            //System.out.println(user.getRole()+" user allowed driver");
                            if (user.getRole().equals("driver")) {
                                System.out.println(user.getRole() + " user is a driver");
                                return true;
                            }
                        } else if(containsOperator) {
                            //System.out.println(user.getRole()+" user allowed operator");
                            if (user.getRole().equals("operator")) {
                                System.out.println(user.getId() + " user is a operator");
                                return true;
                            }
                        } else {
                            System.out.println(user.getRole()+" user allowed admin");
                            if (user.getRole().equals("admin")) {
                                System.out.println(user.getId() + " user is a operator");
                                return true;
                            } else {
                                System.out.println("user is not admin in role column");
                            }
                        }
                    } else {
                        System.out.println("Can not get user id");
                    }
                } else {
                    System.out.println("Can not get any user with the token");
                }
            } else {
                System.out.println("token not found----------------");
                System.out.println(request.getHeader("authorization"));
                System.out.println("--------------------------------");
                response.setStatus(401);
            }
            // if it's in drivers route or operators but doesn't have any of above conditions so it's unauthorized
        }
        System.out.println("[AtuhInceptor:preHandle:end] 401");

//        System.out.println("token not found----------------");
//        System.out.println(request.getHeader("authorization"));
//        System.out.println("--------------------------------");
        response.setStatus(401);
        return false;
//        return true;
    }

    @Override
    public void afterCompletion(HttpServletRequest request,
                                HttpServletResponse response,
                                Object handler,
                                Exception exception)
            throws Exception {

        System.out.println("Inside after completion");
    }

}