package com.controllers.api;

import com.controllers.api.constants.Routes;
import com.controllers.helpers.Responses;
import com.dao.CarDao;
import com.dao.UserDao;
import com.models.Car;
import com.models.User;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;
import java.util.HashMap;
import java.util.List;

@RestController("CarControllerApi")
@Transactional
public class CarController extends BaseController {

    private final UserDao userDao;
    private final CarDao carDao;
    HttpServletRequest req;
//    private final SessionFactory sessionFactory;


    @Autowired
    public CarController(HttpServletResponse res, HttpServletRequest req, UserDao userDao, CarDao carDao, SessionFactory sessionFactory)
    {
        super(userDao, sessionFactory);

        this.userDao = userDao;
        this.carDao= carDao;
        this.req = req;
        this.ar = new ApiResponse(res);

    }

    /**
     * get user cars
     * @return
     */
//    @JsonView(View.Summary.class)
    @GetMapping(Routes.profileRoute + "/cars")
    @Transactional
    public @ResponseBody
    Responses cars(HttpServletRequest req)
    {
        authUser(req);

        String page = getParam(req, "page");
        Query qcars = carDao.get(" where user_id=" + authUser.getId(), page);

        List<Car> cars = qcars.list();

        Integer carCounts = carDao.counts(" where user_id=" + authUser.getId());

        HashMap meta = createMetadata(carCounts, page, itemsPerPage);

        return ar.success(cars, meta);
    }

    /**
     * Create a new car for current user
     * @param car
     * @param err
     * @return
     */
    @PostMapping(Routes.profileRoute+"/cars")
    public @ResponseBody
    Responses createCar(@Validated @RequestBody Car car, BindingResult err)
    {
        if (err.hasErrors()) {
            return ar.formError(err);
        }

        if(carDao.exists(car.getPlateNumber())) {
            return ar.formError(assArray("plateNumber", "this car is registered"));
        }

        authUser(req);

        car.setUser(authUser);
//        System.out.println("@PostMapping(Routes.profileRoute+\"/cars\") car: " + car);
        carDao.save(car);

        if(car.getId() > 0) {
            return ar.success("Successfully added",car);
        }

        return ar.notFoundError("there is a problem creating your car");

    }

    /**
     * Update a users car
     * @param id
     * @param newCar
     * @param err
     * @return
     */
    @PutMapping(path = Routes.profileRoute + "/cars/{id}" ,
            consumes={MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
    public @ResponseBody
    Responses updateCar(@PathVariable @NotNull @DecimalMin("0") String id, @Validated @RequestBody Car newCar, BindingResult err)
    {
        if (err.hasErrors()) {
            return ar.formError(err);
        }

        authUser(req);
//        System.out.println("@PutMapping(Routes.profileRoute + \"/cars/{id}\")  car: "+ newCar);
        Car car = carDao.get(new Integer(id));
        if (car != null) {
            if(car.getUser() != authUser) return ar.unAuthorized();

            if (carDao.existsExept(newCar.getPlateNumber(), car)) {
                return ar.formError(assArray("plateNumber", "this car is registered"));
            }


            try {
                carDao.save(newCar, car.getId());
                return ar.success("Car updated successfully");
            } catch (Exception e) {
                return ar.serverError("Somthing happend try again later");
            }
        }

        return ar.notFoundError("Car not found");

    }

    @DeleteMapping(Routes.profileRoute + "/cars/{id}")
    public @ResponseBody
    Responses deleteCar(@PathVariable @NotNull @DecimalMin("0") String id)
    {
        authUser(req);

        Integer carId = new Integer(id);

        Car car = carDao.get(carId);
        if (car != null) {
            if(car.getUser() != authUser) return ar.unAuthorized();

            int deleted = carDao.delete(carId);
            if(deleted > 0) {
                return ar.success("Car deleted successfully");
            } else {
                return ar.success("There is a problem deleting your car");
            }
        }
        return ar.notFoundError("Car not found");
    }


    /**
     * operator section
     * */

    @GetMapping(Routes.operatorRoute + "/searchPlate")
    public @ResponseBody Responses searchPlate(@RequestParam String plateNumber, HttpServletRequest req)
    {
        authUser(req);
//        String plateNumber = car.getPlateNumber();
//        String plateNumber = getParam(req, "plateNumber");
//        out(newUser.getPlateNumber());
//        out(authUser);

        String queryString = " where plate_number=:plate";
        out(queryString);
        Query query = carDao.get(queryString);
        query.setParameter("plate", plateNumber);

        List<Car> cars = query.list();
        if(cars.size() > 0) {
            return ar.success(cars.get(0));
        }
        return ar.notFoundError("There is no user with this plate number");
    }
}
