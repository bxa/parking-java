package com.controllers.api;


import com.controllers.helpers.Responses;
import com.dao.UserDao;
import com.models.User;
import com.mysql.cj.util.StringUtils;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCrypt;

import javax.servlet.http.HttpServletResponse;


import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.validation.Valid;
import java.util.HashMap;


@Controller("RegisterControllerApi")
@RequestMapping("/api/v.1")
//@Validated
public class RegisterController {

    private final UserDao userDao;
    public ApiResponse ar;

    @Autowired
    public RegisterController(UserDao userDao, HttpServletResponse res,  SessionFactory sessionFactory) {
        this.userDao = userDao;
        this.ar = new ApiResponse(res);
    }

    @PostMapping("/checkRes")
    public @ResponseBody
    Responses checkRes()
    {
        return ar.serverError("this is a server error");
    }

    // getCode(mobile)
    @PostMapping("/login")
    public @ResponseBody
    Responses getCode(@Validated @RequestBody User user1, BindingResult errors)
    {
        if (errors.hasErrors()) {
            return ar.formError(errors);
        }

        User user = new User();
        HelperController helper = new HelperController();

        // check if user exists
        String[] keyValue= {"mobile", user1.getMobile()};
        user = userDao.findOrCreate(keyValue, userDao);

        // Generate a 5 digit code for mobile confirmation and create a hash from it
        Integer confirmationCode = helper.generateConfirmationCode();
        String hashCode = helper.generateHashFromInt(confirmationCode);

        // Set and save user
        try {
            user.setConfirmMobile(hashCode);
            userDao.save(user, user);
        } catch (Exception e) {
            ar.serverError("There is a problem with saving your data please try again later.");
        }

        // send confirmation code via SMS
        if(helper.sendConfirmationCodeSMS(confirmationCode)) {
            ar.success("We've sent a 5 digit code to your phone number!");
        } else {
            ar.serverError("Problem with sending code! please try again later.");
        }
        return ar;
    }


    @PostMapping("/code")
    public @ResponseBody
    Responses checkCode(@Valid @RequestBody User user1, BindingResult errors, HttpServletResponse response)
    {
        if (errors.hasErrors()) {
            return ar.formError(errors);
        }
        User user = new User();
        System.out.println(user1.getMobile());
        user = (User) userDao.findBy("mobile", user1.getMobile());
//        user =(User) userDao.get(" WHERE mobile = "+user1.getMobile()).list().get(0);
        if(user != null) {
//            System.out.println(user.getConfirmMobile(), BCrypt.checkpw(user1.getConfirmMobile(), user.getConfirmMobile()));
            // check if requested code and database code maches

            if(!user.getConfirmMobile().isEmpty() && BCrypt.checkpw(user1.getConfirmMobile(), user.getConfirmMobile())) {
                // create user token
                HashMap res = new HashMap();
                String hash = BCrypt.hashpw(user.getMobile()+user.getId()+user.getConfirmMobile(), BCrypt.gensalt());
                System.out.println("confirmation code is not empty!");
                //set hash and clear confirm mobile field
                user.setToken("Bearer "+hash);
                user.setConfirmMobile("");
                userDao.save(user, user);
                res.put("user", user);
                res.put("token", hash);

                ar.success(res);
            } else {
                ar.notFoundError("Code not found");
            }
        } else {
            ar.notFoundError("Mobile not found");
        }
        return ar;

    }
}
