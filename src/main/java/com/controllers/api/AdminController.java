package com.controllers.api;

import com.controllers.api.constants.Routes;
import com.controllers.helpers.Responses;
import com.dao.UserDao;
import com.models.User;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@RestController("AdminControllerApi")
public class AdminController extends BaseController {
    private final UserDao userDao;
    HttpServletRequest req;
    //    private final SessionFactory sessionFactory;


    @Autowired
    public AdminController(HttpServletResponse res, HttpServletRequest req, UserDao userDao, SessionFactory sessionFactory)
    {
        super(userDao, sessionFactory);

        this.userDao = userDao;
        this.req = req;
        this.ar = new ApiResponse(res);

    }

    @RequestMapping(Routes.adminRoute)
    public @ResponseBody
    Responses check()
    {
        User user = new User();

        ar.success("ssss");

        return ar;
    }
}
