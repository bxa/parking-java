package com.controllers.api;

import com.controllers.api.constants.Routes;
import com.controllers.helpers.Responses;
import com.dao.LotDao;
import com.dao.ReportDao;
import com.dao.UserDao;
import com.dao.OrderDao;
//import org.apache.lucene.util.QueryBuilder;
import com.models.Lot;
import com.models.Order;
import com.models.Report;
import com.models.enums.Type;
import org.hibernate.SessionFactory;
//import org.hibernate.search.query.dsl.QueryBuilder;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

//import javax.persistence.EntityManager;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;


@RestController("ReportControllerApi")
public class ReportController extends BaseController {

    private final UserDao userDao;
    private final LotDao lotDao;
    private final ReportDao reportDao;
    private final OrderDao orderDao;
    HttpServletRequest req;
    //    private final SessionFactory sessionFactory;
//    private EntityManager em;

    @Autowired
    public ReportController(HttpServletResponse res, HttpServletRequest req, UserDao userDao, LotDao lotDao, SessionFactory sessionFactory, ReportDao reportDao, OrderDao orderDao)
    {
        super(userDao, sessionFactory);
//        this.em = em;
        this.userDao = userDao;
        this.lotDao= lotDao;
        this.req = req;
        this.orderDao = orderDao;
        this.ar = new ApiResponse(res);
        this.reportDao = reportDao;

    }

    /**
     * report a lot
     * @param id
     * @param newReport
     * @param err
     * @return
     */
    @PostMapping(Routes.eventRoute + "/{id}/report")
    @Transactional
    public @ResponseBody
    Responses sendReport(@PathVariable @NotNull @DecimalMin("0") String id, @Validated @RequestBody Report newReport, BindingResult err)
    {
        if (err.hasErrors()) {
            return ar.formError(err);
        }
        authUser(req);

        int orderId = new Integer(id);
        if (!(orderId > 0)) {
            return ar.formError(assArray("id", "Id is not valid"));
        }
//        if(reportDao.exists(new String[]{"subject", "text"}, new String[]{newReport.getSubject(), newReport.getText()})) {
//            return ar.errorFormMessage("This report exists");
//        }

        Order order = orderDao.get(orderId);

        if (order.getId() > 0) {
            newReport.setItem(order);
            newReport.setUser(authUser);
            newReport.setType(Type.order);
            reportDao.save(newReport);
            if(newReport.getId() > 0) {
                return ar.success(newReport);
            }
            return ar.serverError();
        }
        return ar.notFoundError("This order not found");
    }


    @GetMapping(Routes.profileRoute + "/reports")
    @Transactional
    public @ResponseBody
    Responses getReports(HttpServletRequest req)
    {
        authUser(req);

        String page = getParam(req, "page");
        Query query = reportDao.get(" where user_id=" + authUser.getId(), page);

        List<Report> items = query.list();

        Integer counts = reportDao.counts(" where user_id=" + authUser.getId());

        HashMap meta = createMetadata(counts, page, itemsPerPage);

        return ar.success(items, meta);
    }


    /**
     * get operator one order details
     * @param id
     * @param req
     * @return
     */
    @GetMapping(Routes.profileRoute + "/reports/{id}")
    @Transactional
    public @ResponseBody Responses getReport(@PathVariable String id, HttpServletRequest req)
    {
        authUser(req);

        int itemId = new Integer(id);
        if (!(itemId > 0)) {
            return ar.formError(assArray("id", "Id is not valid"));
        }


        Report report = reportDao.get(itemId);

        if(report.getUser().getId() == authUser.getId()) {
            return ar.success(report);
        }

        return ar.serverError();
    }
}
