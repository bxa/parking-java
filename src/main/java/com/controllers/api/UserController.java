package com.controllers.api;

import com.controllers.api.constants.Routes;
import com.controllers.helpers.Responses;
import com.dao.CarDao;
import com.dao.UserDao;
import com.models.User;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

/**
 * @author Behrooz
 *
 */
@RestController("UserControllerApi")

public class UserController extends BaseController {

    private final UserDao userDao;
    private final CarDao carDao;
    HttpServletRequest req;

    @Autowired
    public UserController(HttpServletResponse res, HttpServletRequest req, UserDao userDao, CarDao carDao, SessionFactory sessionFactory)
    {
        super(userDao, sessionFactory);
        this.userDao = userDao;
        this.carDao= carDao;
        this.req = req;
        this.ar = new ApiResponse(res);
//        this.req = req;
    }
    /**
     * Check token and if it is ok return the user
     */
    @GetMapping("/checkToken")
    public @ResponseBody
    Responses checkToken(HttpServletRequest req)
    {
        authUser(req);

        return ar.success(authUser);
    }

    /**
     * Get current tokens user
     */
    @PostMapping("/profile/current")
    public @ResponseBody
    Responses current()
    {
        authUser(req);

        HashMap res = assArray("user", authUser);
        return ar.success(res);
    }

    /**
     * Get current tokens user profile information
     */
    @GetMapping(Routes.profileRoute)
    public @ResponseBody
    Responses getProfile()
    {
        authUser(req);
        HashMap res = assArray("user", authUser);
        return ar.success(authUser);
    }
    /**
     * update current authenticated user profile
//     * @param user
     * @param err
     * @return
     */
    @PutMapping(Routes.profileRoute)
    @Transactional
    public @ResponseBody
    Responses updateProfile(@Validated @RequestBody User newUser, BindingResult err)
    {
        if (err.hasErrors()) {
            return ar.formError(err);
        }
        authUser(req);

        try {
            userDao.save(newUser, authUser);
            authUser(req);
            return ar.success("Profile updated successfully!", authUser);
        } catch (Exception e) {
            return ar.serverError("Somthing happend try again later");
        }
    }

    /**
     * operator section
     * */

    @PostMapping(Routes.operatorRoute + "/searchPlate")
    @Transactional
    public @ResponseBody Responses users(@RequestBody User newUser, HttpServletRequest req)
    {
        authUser(req);
        out(authUser);

        String queryString = " where plate_number=:plate";

        Query query = userDao.get(queryString);

        List<User> users = query.list();
        if(users.size() > 0) {
            return ar.success(users.get(0));
        }
        return ar.notFoundError("There is no user with this plate number");
    }

    /**
     * ADMIN section
     * */

    @GetMapping(Routes.adminRoute + "/users")
    @Transactional
    public @ResponseBody Responses users(HttpServletRequest req)
    {
        authUser(req);

        out(authUser);
        String page = getParam(req, "page");
//        String queryString = " where plate_number=:plate";

        Query query = userDao.get("", page);
//        query.setParameter("plate", newUser.getPlateNumber());

        List<User> users = query.list();
        if(users.size() > 0) {
            return ar.success(users.get(0));
        }
        return ar.notFoundError("There is No user yet");
    }
}
