package com.controllers.api;

import com.controllers.api.constants.Routes;
import com.controllers.helpers.RequestHelper;
import com.controllers.helpers.Responses;
import com.dao.*;
import com.models.Car;
import com.models.Lot;
import com.models.Order;
import com.models.Spot;
import com.models.enums.Status;
import com.models.filters.OrderFilter;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@RestController("OrderControllerApi")
@Transactional
public class OrderController extends BaseController {

    private final UserDao userDao;
    private final OrderDao orderDao;
    private final CarDao carDao;
    private final SpotDao spotDao;
    HttpServletRequest req;
    //    private final SessionFactory sessionFactory;

    @Autowired
    public OrderController(HttpServletResponse res, HttpServletRequest req, UserDao userDao, OrderDao orderDao, SpotDao spotDao, SessionFactory sessionFactory, CarDao carDao)
    {
        super(userDao, sessionFactory);

        this.userDao = userDao;
        this.orderDao= orderDao;
        this.spotDao= spotDao;
        this.req = req;
        this.carDao = carDao;
        this.ar = new ApiResponse(res);
    }

    /**
     * get user orders
     * @return
     */
//    @JsonView(View.Summary.class)
    @GetMapping(Routes.profileRoute + "/orders")
    @Transactional
    public @ResponseBody
    Responses orders(HttpServletRequest req)
    {
        authUser(req);

        String page = getParam(req, "page");
        Query qorders = orderDao.get(" where user_id=" + authUser.getId(), page);

        List<Order> orders = qorders.list();

        Integer orderCounts = orderDao.counts(" where user_id=" + authUser.getId());

        HashMap meta = createMetadata(orderCounts, page, itemsPerPage);

        return ar.success(orders, meta);
    }

    /**
     * Create a new order for current user
     * @param spotId
     * @return
     */
    @PostMapping(Routes.eventRoute+"/start")
    @Transactional
    public @ResponseBody
    Responses createOrder(@RequestParam String spotId)
    {
        int id = new Integer(spotId);
        if (!(id > 0)) {
            return ar.formError(assArray("id", "Id is not valid"));
        }

        Spot spot = spotDao.get(id);

        authUser(req);

        System.out.println("[OrderController:createOrder] Spot: " + spot);
        if (spot != null) {
            if(spot.getStatus() == Status.free) {
                Order order = new Order();
                order.setStartTime(new Date());
                order.setUser(authUser);
                order.setLot(spot.getLot());
                order.setSpot(spot);
                order.setType(0);
                order.setStatus(0);

                spot.setStatus(Status.busy);

                spotDao.save(spot, spot);

                orderDao.save(order);
                return ar.success(order);
            } else {
                return ar.formError(assArray("spot", "This spot is busy"));
            }
        }


        return ar.notFoundError("there is a problem creating your order");
    }

    @PutMapping(Routes.eventRoute+"/end")
    @Transactional
    public @ResponseBody
    Responses endOrder(@RequestParam String eventId)
    {
        int id = new Integer(eventId);
        if (!(id > 0)) {
            return ar.formError(assArray("id", "Id is not valid"));
        }

        Order order = orderDao.get(id);

        if (order != null) {
//            System.out.println("[OrderController:endOrder]"+order.getSpot().getStatus());
//            System.out.println((new Date().getTime() - order.getStartTime().getTime()));
            // change spot status to free
            Spot spot = spotDao.get(order.getSpot().getId());
            spot.setStatus(Status.free);
            spotDao.save(spot, spot);

            // create a price and change the order type and end time
            order.setPrice((new Date().getTime() - order.getStartTime().getTime()) / 60000);
            order.setEndTime(new Date());
            order.setStatus(1);

            orderDao.save(order, order);
            return ar.success(order);
//            } else {
//                return ar.formError(assArray("spotId", "Spot is not free"));
//            }
//            Timestamp ots = new Timestamp(order.getStartTime());
//            order.setPrice(ts - );

        }
        return ar.serverError();
    }


    /**
     * operator section
     * */

    /**
     * get orders that are in progress
     * @param req
     * @return
     */
    @GetMapping(Routes.operatorRoute + "/events")
    @Transactional public @ResponseBody Responses parkEvents(HttpServletRequest req)
    {
        authUser(req);
        String page = getParam(req, "page");
        String spotId = getParam(req, "spotId");
        String lotId = getParam(req, "lotId");
//        String startDate = getParam(req, "startDate");
//        String endDate = getParam(req, "endDate");

        Set<Lot> userLots = authUser.getLots();
        String lotIds = userLots.stream().map(pLot -> String.valueOf(pLot.getId())).collect(Collectors.joining(","));


        String queryString = "select * from orders where lot_id in(" + lotIds + ") and exists (select * from spots where orders.spot_id=spots.id) order by id desc";
        if(!spotId.isEmpty())
            queryString = "select * from orders where lot_id in(" + lotIds + ") and spot_id=" + spotId + " and exists (select * from spots where orders.spot_id=spots.id) order by id desc";
        if(!lotId.isEmpty())
            queryString = "select * from orders where lot_id in(" + lotIds + ") and lot_id=" + lotId + " and exists (select * from spots where orders.spot_id=spots.id) order by id desc";

        Query qOrders = (Query) orderDao.getQuery(queryString, page);
        List<Order> orders = qOrders.list();

        Query qCount = orderDao.countsQuery(queryString);
        int orderCounts =  qCount.getResultList().size();

        HashMap meta = createMetadata(orderCounts, page, itemsPerPage);

        return ar.success(orders, meta);
    }


    /**
     * get operator orders
     * @param request
     * @return
     */
    @GetMapping(Routes.operatorRoute + "/orders")
    @Transactional
    public @ResponseBody Responses operatorOrders(HttpServletRequest request)
    {
        authUser(req);
        String page = getParam(req, "page");

        Set<Lot> userLots = authUser.getLots();
        String lotIds = userLots.stream().map(pLot -> String.valueOf(pLot.getId())).collect(Collectors.joining(","));

        RequestHelper rh = new RequestHelper(req);
//        System.out.println(rh.getAllParams());
        String where = "";
        if(rh.notEmpty("startTime")) {
            where += " and created_at > FROM_UNIXTIME(" + rh.get("startTime")+")";
        }

        if(rh.notEmpty("endTime")) {
            where += " and created_at < FROM_UNIXTIME(" + rh.get("endTime")+")";
        }

        String queryString = "select * from orders where lot_id in(" + lotIds + ") "+where+" order by id desc";
        out("this is query string :::::::::: "+queryString);

        Query qOrders = (Query) orderDao.getQuery(queryString, page);
        List<Order> orders = qOrders.list();

        Query qCount = orderDao.countsQuery(queryString);
        int orderCounts =  qCount.getResultList().size();

        HashMap meta = createMetadata(orderCounts, page, itemsPerPage);

        return ar.success(orders, meta);
    }

    /**
     * get operator one order details
     * @param id
     * @param req
     * @return
     */
    @GetMapping(Routes.operatorRoute + "/orders/{id}")
    @Transactional
    public @ResponseBody Responses operatorOneOrder(@PathVariable String id, HttpServletRequest req)
    {
        authUser(req);

        int orderId = new Integer(id);
        if (!(orderId > 0)) {
            return ar.formError(assArray("id", "Id is not valid"));
        }

        Set<Lot> userLots = authUser.getLots();
        List<Integer> ids = userLots.stream().map(Lot::getId).collect(Collectors.toList());

        Order order = orderDao.get(orderId);
        if(order != null) {
            if(ids.contains(order.getLotId())) {
                return ar.success(order);
            } else {
                return ar.unAuthorized();
            }
        } else {
            return ar.notFoundError("Order not found");
        }

    }


    /**
     * Create a new order for current user
     * @param spotId
     * @param carId
     * @return OK
     */
    @PostMapping(Routes.operatorRoute+Routes.eventRoute+"/start")
    @Transactional
    public @ResponseBody
    Responses startEvent(@RequestParam String spotId, @RequestParam String carId)
    {
        int id = new Integer(spotId);
        if (!(id > 0)) {
            return ar.formError(assArray("SpotId", "SpotId is not valid"));
        }
        int car_id = new Integer(carId);
        if (!(car_id > 0)) {
            return ar.formError(assArray("CarId", "CarId is not valid"));
        }

        Spot spot = spotDao.get(id);

        Car car = carDao.get(car_id);

        authUser(req);
        Set<Lot> userLots = authUser.getLots();
        List<Integer> ids = userLots.stream().map(Lot::getId).collect(Collectors.toList());
        if (spot != null) {
            if(ids.contains(spot.getLot().getId())) {
                if(spot.getStatus() == Status.free) {
                    Order order = new Order();
                    order.setStartTime(new Date());
                    order.setUser(car.getUser());
                    order.setLot(spot.getLot());
                    order.setSpot(spot);
                    order.setType(0);
                    order.setStatus(0);

                    spot.setStatus(Status.busy);

                    spotDao.save(spot, spot);

                    orderDao.save(order);
                    return ar.success(order);
                } else {
                    return ar.success("Can not park in this spot, the spot is: "+ spot.getStatus());
                }
            } else {
                return ar.unAuthorized();
            }
        }

        return ar.success("there is a problem creating your order, spot is not available");
    }


    @PutMapping(Routes.operatorRoute+Routes.eventRoute+"/end")
    @Transactional
    public @ResponseBody
    Responses endEvent(@RequestParam String spotId)
    {
        int id = new Integer(spotId);
        if (!(id > 0)) {
            return ar.formError(assArray("spotId", "Id is not valid"));
        }
        authUser(req);
        Set<Lot> userLots = authUser.getLots();
        List<Integer> ids = userLots.stream().map(Lot::getId).collect(Collectors.toList());
        Spot spot = spotDao.get(id);
//        out(spot.toString());

        if(spot.getStatus() != Status.busy) return ar.formError(assArray("Status", "This spot is not busy"));

        if(!ids.contains(spot.getLot().getId()))  return ar.unAuthorized();

//        Order order = (Order) orderDao.get(" where spot_id=" + id + " and status=0 ").list().get(0);
//        out(order);
//        List<Order> orders = qOrders.list();
        Query q = orderDao.get("where spot_id="+ id + " and status=0 ");
        List<OrderFilter>  orders = q.list();
        if (orders.size() == 1) {
            spot.setStatus(Status.free);
            spotDao.save(spot, spot);
            Order order = orderDao.get(orders.get(0).getId());
//
//            // create a price and change the order type and end time
            order.setPrice((new Date().getTime() - order.getStartTime().getTime()) / 60000);
            order.setEndTime(new Date());
            order.setStatus(1);
//
            orderDao.save(order, order);
            return ar.success(order);
//            } else {
//                return ar.formError(assArray("spotId", "Spot is not free"));
//            }
//            Timestamp ots = new Timestamp(order.getStartTime());
//            order.setPrice(ts - );
        }else if(orders.size() > 1) {
            return ar.notFoundError("Many orders");
        } else {
            return ar.notFoundError("There is no order. you should start");
        }
//        return ar.serverError();
    }
}
