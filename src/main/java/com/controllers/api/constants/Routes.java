package com.controllers.api.constants;

public final class Routes {
//    private Routes(){}
    public static final String profileRoute  = "/profile";
    public static final String eventRoute  = "/events";
    public static final String operatorRoute = "/operator";
    public static final String adminRoute    = "/admin";
}
