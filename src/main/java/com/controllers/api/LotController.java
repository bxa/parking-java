package com.controllers.api;

import com.controllers.api.constants.Routes;
import com.controllers.helpers.RequestHelper;
import com.controllers.helpers.Responses;
import com.dao.LotDao;
import com.dao.UserDao;
import com.models.Lot;
//import org.apache.lucene.util.QueryBuilder;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
//import org.hibernate.search.query.dsl.QueryBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

//import javax.persistence.EntityManager;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;
import java.util.HashMap;
import java.util.List;

@RestController("LotControllerApi")
@Transactional
public class LotController extends BaseController {

    private final UserDao userDao;
    private final LotDao lotDao;
    HttpServletRequest req;
    //    private final SessionFactory sessionFactory;
//    private EntityManager em;

    @Autowired
    public LotController(HttpServletResponse res, HttpServletRequest req, UserDao userDao, LotDao lotDao, SessionFactory sessionFactory)
    {
        super(userDao, sessionFactory);
//        this.em = em;
        this.userDao = userDao;
        this.lotDao= lotDao;
        this.req = req;
        this.ar = new ApiResponse(res);

    }


    /**
     * get user lots
     * @return
     */
//    @JsonView(View.Summary.class)
    @GetMapping("/lots")
    @Transactional
    public @ResponseBody
    Responses lots(HttpServletRequest req)
    {
        authUser(req);

        String page = getParam(req, "page");
        Query qlots = lotDao.get("", page);

        List<Lot> lots = qlots.list();

        Integer lotCounts = lotDao.counts("");

        HashMap meta = createMetadata(lotCounts, page, itemsPerPage);

        return ar.success(lots, meta);
    }

    /**
     * get a lot by id
     * @param id
     * @param err
     * @return
     */
    @GetMapping("/lots/{id}")
    public @ResponseBody
    Responses lot(@PathVariable @NotNull @DecimalMin("0") String id)
    {
        int lotId = new Integer(id);
        if (!(lotId > 0)) {
            return ar.formError(assArray("id", "Id is not valid"));
        }
        Lot lot = lotDao.get(lotId);
        if (lot != null) {
            return ar.success(lot);
        }
        return ar.notFoundError("Lot not found");
    }
    /**
     * get user lots
     * @return
     */
    @GetMapping("/lots/search")
    @Transactional
    public @ResponseBody
    Responses lotsSearch(HttpServletRequest req)
    {
        authUser(req);
        RequestHelper rh = new RequestHelper(req);

        if(!validateLatLng(rh.get("lat"), rh.get("lng"))) {
            return ar.errorFormMessage("Wrong latitute or longitute");
        }
        
        String query = "select *, ST_distance_sphere(\n" +
                    "ST_GeomFromText(CONCAT('POINT(',lat,' ',lng,')'), 4326),\n" +
                    "ST_GeomFromText('POINT(" + rh.get("lat") + " " + rh.get("lng") +")', 4326)" +
                ") as distance from lots ";

        String where = "";
        if(rh.notEmpty("keyword")) {
            where += " where name like :keyword";
        }

        if(rh.notEmpty("lat")) {
            where += " having distance < :distance";
        }

        query = query + where;
        System.out.println(query);

        Query qLots =(Query) lotDao.getQuery(query, rh.get("page"));

        Query qCount = lotDao.countsQuery(query);

        if(rh.notEmpty("keyword")) {
            qLots.setParameter("keyword", "%"+rh.get("keyword")+"%");
            qCount.setParameter("keyword", "%"+rh.get("keyword")+"%");
        }

        if(rh.notEmpty("distance")) {
            qLots.setParameter("distance", rh.get("distance"));
            qCount.setParameter("distance", rh.get("distance"));
        } else {
            qLots.setParameter("distance", distance);
            qCount.setParameter("distance", distance);
        }

        List<Lot> lots = qLots.getResultList();
        int lotCounts =  qCount.getResultList().size();

        HashMap meta = createMetadata(lotCounts, rh.get("page"), itemsPerPage);

        return ar.success(lots, meta);
    }

    /** operator methods */

    /**
     * get operator lots
     * @param req
     * @return
     */
    @GetMapping(Routes.operatorRoute + "/lots")
    @Transactional
    public @ResponseBody
    Responses operatorLots(HttpServletRequest req)
    {
        authUser(req);
        String page = getParam(req, "page");

        String queryString = "select * from lots where id in (select lot_id from user_lots where user_id=" + authUser.getId() + ")";
        Query qlots = (Query) lotDao.getQuery(queryString, page);

        List<Lot> lots = qlots.list();

        Query qCount = lotDao.countsQuery(queryString);
        int counts =  qCount.getResultList().size();

        HashMap meta = createMetadata(counts, page, itemsPerPage);

        return ar.success(lots, meta);
    }

}
