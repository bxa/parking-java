<#import "/spring.ftl" as spring />
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Create a user</title>
</head>
<body>
    <h1> Create a user</h1> <br>
	<h2>${message!}</h2>
	<hr> <h2>Create a new user</h2>
	<form action="/admin/users/create" method="post">
        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>

		name:
        <@spring.bind "user.name"/>
        <input name="name" placeholder="Name" type="text">
        <@spring.showErrors "<br>"/><br>

		mobile:
        <@spring.bind "user.mobile"/>
        <input name="mobile" placeholder="Mobile" type="text"> <br>
        <@spring.showErrors "<br>"/><br>

		<input value="submit" type="submit">
	</form>
</body>
</html>