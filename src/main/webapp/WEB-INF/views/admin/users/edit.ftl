<#import "/spring.ftl" as spring />
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<h2>${message!}</h2>
	<hr> <h2>Edit ${user.getName()}:</h2>
	<form action="/admin/users/${user.getId()}/edit" method="post">
        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>

		name:
        <@spring.bind "user.name"/>
        <input name="name" value="${user.getName()}" placeholder="Name" type="text">
        <@spring.showErrors "<br>"/><br>
    <#--<errors path="name" cssClass="error"></errors> -->
    <#--<#if name.errorMessages >-->
    <#--<#list spring.name.errorMessages as error> <b>${error}</b> <br> </#list>-->

    <#--</#if>-->
    <#--<#if spring.name.error>-->
    <#--<ul>-->
    <#--<#list spring.name.errors.globalErrors as error>-->
    <#--<li>${error.defaultMessage}</li>-->
    <#--</#list>-->
    <#--</ul>-->
    <#--</#if>-->
        <#--<#if (spring.name.errors.allErrors?size > 0) >-->
            <#--<@spring.message "my.global.error.code"/>-->
        <#--</#if>-->
        <br>

		<#--<input name="email" value="${user.getEmail()}" placeholder="Email" type="text">-->
		mobile: <input name="mobile" value="${user.getMobile()}" placeholder="Mobile" type="text"> <br>
		<input value="submit" type="submit">
	</form>
</body>
</html>