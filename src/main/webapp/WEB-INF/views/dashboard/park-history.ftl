<#assign spring=JspTaglibs["http://www.springframework.org/tags"]/>
<#include "../layout/main.ftl">

<@layout  title='Park History' >
    <h1>Park History</h1>
    <h3>User name: ${authUser.fullname!} - ${authUser.mobile!}</h3>
    <hr>
    <#include "dashboard-sidebar.ftl">

    <div class="contents"></div>
    <#--<#list orders as order>-->
        <#--${order} <br>-->
    <#--</#list>-->
</@layout>

<@footer>
    <script type="text/javascript">
        $.getJSON('/dashboard/park-history/json', function( data ) {
            var all = null;
            $.each( data, function( key, val ) {
                all += ("<li>");
                $.each( val, function( itemkey, itemval ) {
                    all += itemkey + ": " + itemval + " <br> ";
                });
                all += ("</li>");
            });

            $( "<ul/>").html(all).appendTo( ".contents" );
        });
    </script>
</@footer>